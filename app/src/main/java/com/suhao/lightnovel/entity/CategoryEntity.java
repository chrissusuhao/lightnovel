package com.suhao.lightnovel.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.litesuits.orm.db.annotation.Column;
import com.litesuits.orm.db.annotation.PrimaryKey;
import com.litesuits.orm.db.annotation.Table;
import com.litesuits.orm.db.annotation.Unique;
import com.litesuits.orm.db.enums.AssignType;

import lombok.Data;

@Data
@Table("category_entity")
public class CategoryEntity implements Parcelable {
    @PrimaryKey(AssignType.AUTO_INCREMENT)
    private int id;
    @Unique
    @Column("book_id")
    private String bookId;
    @Column("title")
    private String title;
    @Column("author")
    private String author;
    @Column("description")
    private String description;
    @Column("evaluate_point")
    private String evaluatePoint;
    @Column("url")
    private String url;
    @Column("novel_status")
    private String novelStatus;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(bookId);
        parcel.writeString(title);
        parcel.writeString(author);
        parcel.writeString(description);
        parcel.writeString(evaluatePoint);
        parcel.writeString(url);
        parcel.writeString(novelStatus);
    }

    public static final Parcelable.Creator<CategoryEntity> CREATOR = new Creator<CategoryEntity>()  //此为CREATOR方法，此方法名必须为CREATOR，不能为Creator
    {

        @Override
        public CategoryEntity createFromParcel(Parcel data) {
            CategoryEntity ce = new CategoryEntity();
            ce.setBookId(data.readString());
            ce.setTitle(data.readString());
            ce.setAuthor(data.readString());
            ce.setDescription(data.readString());
            ce.setEvaluatePoint(data.readString());
            ce.setUrl(data.readString());
            ce.setNovelStatus(data.readString());
            return ce;
        }

        @Override
        public CategoryEntity[] newArray(int arg0) {
            return new CategoryEntity[arg0];
        }

    };
}
