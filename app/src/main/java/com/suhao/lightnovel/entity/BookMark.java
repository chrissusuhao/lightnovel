package com.suhao.lightnovel.entity;

import java.io.Serializable;

public class BookMark implements Serializable {

    public int chapter;

    public int page;

    public String title;

    public String desc = "";
}
