package com.suhao.lightnovel.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.litesuits.orm.db.annotation.Column;
import com.litesuits.orm.db.annotation.PrimaryKey;
import com.litesuits.orm.db.annotation.Table;
import com.litesuits.orm.db.enums.AssignType;

import lombok.Data;

@Data
@Table("chapter_entity")
public class ChapterEntity implements Parcelable {
    public static boolean isShortStory;
    @PrimaryKey(AssignType.AUTO_INCREMENT)
    private int id;
    @Column("book_id")
    private String bookId;
    @Column("chapter")
    private int chapter;
    @Column("part")
    private String part;
    @Column("title")
    private String title;
    @Column("url")
    private String url;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(bookId);
        parcel.writeInt(chapter);
        parcel.writeString(part);
        parcel.writeString(title);
        parcel.writeString(url);
    }

    public static final Parcelable.Creator<ChapterEntity> CREATOR = new Creator<ChapterEntity>()  //此为CREATOR方法，此方法名必须为CREATOR，不能为Creator
    {

        @Override
        public ChapterEntity createFromParcel(Parcel data) {
            ChapterEntity ce = new ChapterEntity();
            ce.setBookId(data.readString());
            ce.setChapter(data.readInt());
            ce.setPart(data.readString());
            ce.setTitle(data.readString());
            ce.setUrl(data.readString());
            return ce;
        }

        @Override
        public ChapterEntity[] newArray(int arg0) {
            return new ChapterEntity[arg0];
        }

    };
}
