package com.suhao.lightnovel.utils;

import com.suhao.lightnovel.manager.CacheManager;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by bz on 2018/4/18.
 */

public class StringUtil {

    /**
     * http://www.runoob.com/ncd23d/ 取出 ncd23d
     * @param url
     * @return
     */
    public static String fromUrl2BookId(String url) throws MalformedURLException {
        URL _url = new URL(url);
        String[] s = _url.getPath().split("/");
        return s[1].trim();
    }

    public static int fromUrl2Chapter(String url) throws MalformedURLException {
        URL _url = new URL(url);
        String[] s = _url.getPath().split("/");
        return Integer.parseInt(s[2].trim());
    }

    public static String stringArray2String(String[] array) {
        StringBuilder sb = new StringBuilder();
        for (String s : array) {
            sb.append(s);
        }
        return sb.toString();
    }
}
