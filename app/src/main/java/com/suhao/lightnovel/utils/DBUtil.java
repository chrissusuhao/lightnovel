package com.suhao.lightnovel.utils;

import com.litesuits.orm.LiteOrm;
import com.suhao.lightnovel.MyApplication;

public class DBUtil {

    private static final String DOWNLOAD_DB = "light_novel.db";
    private static LiteOrm liteOrm;

    public static LiteOrm getLiteOrm(){

        if (liteOrm == null) {
            liteOrm = LiteOrm.newSingleInstance(MyApplication.getInstance(), DOWNLOAD_DB);
        }
        liteOrm.setDebugged(true);
        return liteOrm;
    }
}
