package com.suhao.lightnovel.utils;

import com.suhao.lightnovel.base.Constant;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 * Created by bz on 2018/4/17.
 */

public class JsoupUtil {

    /**
     * https://yomou.syosetu.com/search.php?
     *
     * @param word  &word= ファンタジー ...
     * @param order &order= notorder ...
     * @param page  &p=2
     * @return
     */
    public static Document novelCategoryWithOrderConnector(String word, String order, String page) throws IOException {
        return config(Jsoup.connect("https://yomou.syosetu.com/search.php?&word=" + word
                + "&order=" + order
                + "&notnizi=1"
                + "&p=" + page));
    }

    public static Document novelCategoryConnector(String word, String page) throws IOException {
        return novelCategoryWithOrderConnector(word, "notorder", page);
    }

    public static Document novelShortStoryConnector(String url) throws IOException {
        return config(Jsoup.connect(Constant.HIRAGANA_TRANSLATE + url));
    }

    public static Document novelCatalogueConnector(String url) throws IOException {
        return config(Jsoup.connect(url));
    }

    public static Document config(Connection connection) throws IOException {
        return connection.timeout(30000).get();
    }
}
