/*
 * Copyright (C) 2013-2015 Dominik Schürmann <dominik@dominikschuermann.de>
 * Copyright (C) 2013-2015 Juha Kuitunen
 * Copyright (C) 2013 Mohammed Lakkadshaw
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.suhao.lightnovel.utils;

import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.Html;

import com.blankj.utilcode.util.LogUtils;

import org.xml.sax.XMLReader;

import java.util.List;

/**
 * Some parts of this code are based on android.text.Html
 */
public class HtmlTagHandler implements Html.TagHandler {
    public static final String DIV = "HTML_TEXTVIEW_ESCAPED_DIV_TAG";

    List<String> kanji;
    List<String> hiragana;
    private int index = 0;

    public HtmlTagHandler(List<String> kanji, List<String> hiragana) {
        this.kanji = kanji;
        this.hiragana = hiragana;
    }

    public String overrideTags(@Nullable String html){

        if (html == null) return null;

        html = html.replace("<div", "<" + DIV);
        html = html.replace("</div>", "</" + DIV + ">");

        return html;
    }

    @Override
    public void handleTag(final boolean opening, final String tag, Editable output, final XMLReader xmlReader) {
        String content = output.toString();
        if (opening) {
            if (tag.equalsIgnoreCase("ruby")) {
                kanji.add(content.substring(index, content.length()));
                hiragana.add("");
                index = content.length();
            }
        } else {
            if (tag.equalsIgnoreCase("rb")) {
                kanji.add(content.substring(index, content.length()));
                index = content.length();
            } else if (tag.equalsIgnoreCase("rt")) {
                hiragana.add(content.substring(index, content.length()));
                index = content.length();
            }  else if (tag.equalsIgnoreCase(DIV)) {
                kanji.add(content.substring(index, content.length()));
                hiragana.add("");
                index = content.length();
            }
        }
    }
} 
