package com.suhao.lightnovel;

import android.app.Application;
import android.content.Context;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.Utils;
import com.litesuits.orm.LiteOrm;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;
import com.suhao.lightnovel.utils.DBUtil;
import com.suhao.lightnovel.utils.SharedPreferencesUtil;
import com.tencent.bugly.crashreport.CrashReport;

import thinkfreely.changemodelibrary.ChangeModeController;

public class MyApplication extends Application {
    private static MyApplication instance;
    private LiteOrm liteOrm;

    private RefWatcher refWatcher;

    public static RefWatcher getRefWatcher(Context context) {
        MyApplication application = (MyApplication) context.getApplicationContext();
        return application.refWatcher;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        refWatcher = LeakCanary.install(this);
        instance = this;
        liteOrm = DBUtil.getLiteOrm();
        Utils.init(this);
        LogUtils.getConfig().setLogSwitch(true);
        initPrefs();
        CrashReport.initCrashReport(getApplicationContext(), "a7b762467d", true);
    }

    public static MyApplication getInstance() {
        return instance;
    }

    public LiteOrm getLiteOrm() {
        return liteOrm;
    }

    /**
     * 初始化SharedPreference
     */
    protected void initPrefs() {
        SharedPreferencesUtil.init(getApplicationContext(), getPackageName() + "_preference", Context.MODE_MULTI_PROCESS);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        ChangeModeController.onDestory();
    }
}
