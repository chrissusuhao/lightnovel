package com.suhao.lightnovel.component;

import com.suhao.lightnovel.module.novel.bookrack.NovelBookrackFragment;
import com.suhao.lightnovel.module.novel.read.ReadActivity;
import com.suhao.lightnovel.module.novel.category.NovelCategoryFragment;

import dagger.Component;

/**
 * Created by bz on 2018/4/18.
 */
@Component
public interface NovelComponent {
    NovelCategoryFragment inject(NovelCategoryFragment fragment);
    NovelBookrackFragment inject(NovelBookrackFragment fragment);
    ReadActivity inject(ReadActivity readActivity);
}
