package com.suhao.lightnovel.activity.web;

import android.app.Activity;

import com.suhao.lightnovel.base.BasePresenter;


/**
 * WebContract
 *
 * Author: nanchen
 * Email: liushilin520@foxmail.com
 * Date: 2017-04-14  14:38
 */

public interface WebContract {

    interface IWebView {
        Activity getWebViewContext();

        void setGankTitle(String title);

        void loadGankUrl(String url);

        void initWebView();
    }

    interface IWebPresenter extends BasePresenter {
        String getGankUrl();
    }
}
