package com.suhao.lightnovel.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.SnackbarUtils;
import com.blankj.utilcode.util.SpanUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.suhao.lightnovel.MyApplication;
import com.suhao.lightnovel.R;
import com.suhao.lightnovel.base.BaseActivity;
import com.suhao.lightnovel.base.Constant;
import com.suhao.lightnovel.manager.SettingManager;
import com.suhao.lightnovel.manager.communication.FunctionManager;
import com.suhao.lightnovel.manager.communication.FunctionWithParamNoResult;
import com.suhao.lightnovel.module.ViewPagerAdapter;
import com.suhao.lightnovel.module.novel.bookrack.NovelBookrackFragment;
import com.suhao.lightnovel.module.novel.category.NovelCategoryFragment;
import com.suhao.lightnovel.module.novel.read.ReadActivity;
import com.suhao.lightnovel.service.DownloadBookService;
import com.suhao.lightnovel.ui.view.SlidingTabLayout;
import com.suhao.lightnovel.ui.view.dialog.ColorPickerDialog;
import com.suhao.lightnovel.utils.SharedPreferencesUtil;
import com.suhao.lightnovel.utils.StatusBarCompat;

import java.io.File;

import butterknife.BindView;
import thinkfreely.changemodelibrary.ChangeModeController;

public class HomeActivity extends BaseActivity {
    public static final String FUNC_THEME_COLOR_CHANGE = "theme_color_change";
    public static final String FUNC_BOOKRACK_EMPTY = "bookrack_empty";
    public static final String FUNC_BOOKRACK_SNACK_BAR = "bookrack_snack_bar";

    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    @BindView(R.id.navdrawer)
    ListView mDrawerList;
    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.common_toolbar)
    Toolbar toolbar;
    @BindView(R.id.sliding_tabs)
    SlidingTabLayout slidingTabLayout;

    ActionBarDrawerToggle drawerToggle;

    private ArrayAdapter<String> mDrawerListAdapter;
    private ViewPagerAdapter viewPagerAdapter;
    String[] novelCategoryTitles;
    String[] novelBookrackTitles;

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.activity_home;
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_ab_drawer);
        toolbar.setOnMenuItemClickListener(onMenuItemClick);

        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), this, viewPager, slidingTabLayout);
        viewPager.setAdapter(viewPagerAdapter);
        novelCategoryTitles = MyApplication.getInstance().getString(R.string.tab)
                .replace("\r\n", "")
                .replace("\n", "")
                .split(",");
        novelBookrackTitles = new String[]{MyApplication.getInstance().getString(R.string.bookrack)};

        for (int i = 0; i < novelBookrackTitles.length; i++) {
            addBookrackPage(novelBookrackTitles[i], i);
        }
        viewPagerAdapter.notifyDataSetChanged();
        viewPager.setCurrentItem(SettingManager.getInstance().getCategoryPagePosition());
        viewPager.addOnPageChangeListener(pageChangeListener);
        slidingTabLayout.setViewPager(viewPager);
        slidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return Color.WHITE;
            }
        });

        drawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar,
                R.string.app_name, R.string.app_name);
        mDrawerLayout.addDrawerListener(drawerToggle);
        mDrawerLayout.setScrimColor(Color.TRANSPARENT);

        String[] values = new String[]{
                getString(R.string.custom_theme), getString(R.string.novel_category),
                getString(R.string.bookrack), SharedPreferencesUtil.getInstance().getBoolean(Constant.ISNIGHT) ?
                getString(R.string.menu_main_day_mode) :
                getString(R.string.menu_main_night_mode),
                getString(R.string.book_read_settings),
        };
        mDrawerListAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, values);
        mDrawerList.setAdapter(mDrawerListAdapter);
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                switch (position) {
                    case 0:
                        //自定义主题
                        ColorPickerDialog colorPickerDialog = new ColorPickerDialog();
                        colorPickerDialog.show(getSupportFragmentManager(), "colorPickerDialog");
                        break;
                    case 1:
                        //小说列表
                        novelListShow();
                        break;
                    case 2:
                        //我的书架
                        bookrackShow();
                        break;
                    case 3:
                        //设置夜间模式
                        if (SharedPreferencesUtil.getInstance().getBoolean(Constant.ISNIGHT)) {
                            ((TextView) view).setText(getString(R.string.menu_main_night_mode));
                            ChangeModeController.changeNight(HomeActivity.this, R.style.DayTheme);
                            initTheme();
                            SharedPreferencesUtil.getInstance().putBoolean(Constant.ISNIGHT, false);
                        } else {
                            ((TextView) view).setText(getString(R.string.menu_main_day_mode));
                            ChangeModeController.changeNight(HomeActivity.this, R.style.NightTheme);
                            SharedPreferencesUtil.getInstance().putBoolean(Constant.ISNIGHT, true);
                        }
                        recreate();
                        break;
                    case 4:
                        //设置
                        SettingActivity.startActivity(HomeActivity.this);
                        break;

                }

            }
        });
        initCommunication();
        startService(new Intent(this, DownloadBookService.class));
    }

    private void bookrackShow() {
        viewPager.addOnPageChangeListener(null);
        viewPagerAdapter.removeAllFrag();
        viewPagerAdapter.notifyDataSetChanged();
        for (int i = 0; i < novelBookrackTitles.length; i++) {
            addBookrackPage(novelBookrackTitles[i], i);
        }
        viewPagerAdapter.notifyDataSetChanged();
        mDrawerLayout.closeDrawer(Gravity.START);
    }

    private void novelListShow() {
        viewPager.addOnPageChangeListener(pageChangeListener);
        viewPagerAdapter.removeAllFrag();
        viewPagerAdapter.notifyDataSetChanged();
        for (int i = 0; i < novelCategoryTitles.length; i++) {
            addCategoryPage(novelCategoryTitles[i], i);
        }
        viewPagerAdapter.notifyDataSetChanged();
        viewPager.setCurrentItem(SettingManager.getInstance().getCategoryPagePosition());
        mDrawerLayout.closeDrawer(Gravity.START);
    }

    public void addBookrackPage(String pagename, int position) {
        viewPagerAdapter.addFrag(NovelBookrackFragment.newInstance(position), pagename);
        if (viewPagerAdapter.getCount() > 0) slidingTabLayout.setViewPager(viewPager);
    }

    public void addCategoryPage(String pagename, int position) {
        viewPagerAdapter.addFrag(NovelCategoryFragment.newInstance(position), pagename);
        if (viewPagerAdapter.getCount() > 0) slidingTabLayout.setViewPager(viewPager);
    }

    private ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            SettingManager.getInstance().saveCategoryPagePosition(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    private void initTheme() {
        int color = SettingManager.getInstance().getThemeColor();
        statusBarView = StatusBarCompat.compat(this, color);
        mDrawerList.setBackgroundColor(color);
        toolbar.setBackgroundColor(color);
        slidingTabLayout.setBackgroundColor(color);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(Gravity.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (SharedPreferencesUtil.getInstance().getBoolean(Constant.ISNIGHT)) {
            ChangeModeController.changeNight(HomeActivity.this, R.style.NightTheme);
        } else {
            ChangeModeController.changeNight(HomeActivity.this, R.style.DayTheme);
            initTheme();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isFinishing()) {
            FunctionManager.getInstance().removeAllFunctions();
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    /**************组件通讯*************/

    private void initCommunication() {
        FunctionManager.getInstance().addFunction(themeColorChange);
        FunctionManager.getInstance().addFunction(bookrackEmpty);
        FunctionManager.getInstance().addFunction(showBookrackSnackBar);
    }

    private FunctionWithParamNoResult<Integer> themeColorChange = new FunctionWithParamNoResult<Integer>(FUNC_THEME_COLOR_CHANGE) {

        @Override
        public void function(Integer color) {
            statusBarView = StatusBarCompat.compat(HomeActivity.this, color);
            mDrawerList.setBackgroundColor(color);
            toolbar.setBackgroundColor(color);
            slidingTabLayout.setBackgroundColor(color);
        }
    };

    private FunctionWithParamNoResult<Boolean> bookrackEmpty = new FunctionWithParamNoResult<Boolean>(FUNC_BOOKRACK_EMPTY) {

        @Override
        public void function(Boolean isEmpty) {
            if (isEmpty) {
                ToastUtils.showShort(R.string.bookrack_empty);
                novelListShow();
            }
        }
    };

    private FunctionWithParamNoResult<Boolean> showBookrackSnackBar = new FunctionWithParamNoResult<Boolean>(FUNC_BOOKRACK_SNACK_BAR) {

        @Override
        public void function(Boolean isAddBookrack) {
            if (isAddBookrack) {
                SnackbarUtils.with(findViewById(android.R.id.content))
                        .setMessage(getMsg(R.string.add_bookrack_success))
                        .setMessageColor(Color.WHITE)
                        .setBgColor(SettingManager.getInstance().getThemeColor())
                        .setDuration(SnackbarUtils.LENGTH_LONG)
                        .setAction(getString(R.string.goto_bookrack), Color.YELLOW, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                bookrackShow();
                            }
                        })
                        .show();
            } else {
                SnackbarUtils.with(findViewById(android.R.id.content))
                        .setMessage(getMsg(R.string.remove_bookrack_success))
                        .setMessageColor(Color.WHITE)
                        .setBgColor(SettingManager.getInstance().getThemeColor())
                        .setDuration(SnackbarUtils.LENGTH_SHORT)
                        .show();
            }
        }
    };

    /**************组件通讯*************/

    private SpannableStringBuilder getMsg(@StringRes int resId) {
        return new SpanUtils()
                .appendImage(R.mipmap.bookrack, SpanUtils.ALIGN_CENTER)
                .appendSpace(64)
                .append(getString(resId)).setFontSize(18, true)
                .create();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    private Toolbar.OnMenuItemClickListener onMenuItemClick = new Toolbar.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.action_share:
                    shareMsg(getString(R.string.app_name), getString(R.string.app_name), "填入下载链接",
                            null);
                    break;
            }
            return true;
        }
    };

    public void shareMsg(String activityTitle, String msgTitle, String msgText,
                         String imgPath) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        if (TextUtils.isEmpty(imgPath)) {
            intent.setType("text/plain"); // 纯文本
        } else {
            File f = new File(imgPath);
            if (f != null && f.exists() && f.isFile()) {
                intent.setType("image/jpg");
                Uri u = Uri.fromFile(f);
                intent.putExtra(Intent.EXTRA_STREAM, u);
            }
        }
        intent.putExtra(Intent.EXTRA_SUBJECT, msgTitle);
        intent.putExtra(Intent.EXTRA_TEXT, msgText);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(Intent.createChooser(intent, activityTitle));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DownloadBookService.cancel();
        stopService(new Intent(this, DownloadBookService.class));
    }
}
