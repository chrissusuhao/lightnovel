/**
 * Copyright 2016 JustWayward Team
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.suhao.lightnovel.activity;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.suhao.lightnovel.R;
import com.suhao.lightnovel.base.BaseActivity;
import com.suhao.lightnovel.base.Constant;
import com.suhao.lightnovel.manager.CacheManager;
import com.suhao.lightnovel.manager.SettingManager;
import com.suhao.lightnovel.utils.SharedPreferencesUtil;
import com.suhao.lightnovel.utils.StatusBarCompat;

import java.net.URLEncoder;

import butterknife.BindView;
import butterknife.OnClick;
import thinkfreely.changemodelibrary.ChangeModeController;

public class SettingActivity extends BaseActivity {

    public static final String ALIPAY_PERSON = "HTTPS://QR.ALIPAY.COM/FKX00070DNACW909ZYMB76";//个人(支付宝里面我的二维码)

    public static void startActivity(Context context) {
        context.startActivity(new Intent(context, SettingActivity.class));
    }

    @BindView(R.id.mTvSort)
    TextView mTvSort;
    @BindView(R.id.tvFlipStyle)
    TextView mTvFlipStyle;
    @BindView(R.id.tvCacheSize)
    TextView mTvCacheSize;
    @BindView(R.id.common_toolbar)
    Toolbar toolbar;
    @BindView(R.id.ali_pay)
    RelativeLayout aliPay;

    String[] bookrackSortWay;
    String[] readStyle;


    @Override
    protected int getContentViewLayoutID() {
        return R.layout.activity_setting;
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        initToolBar();
        initDatas();
        if (SharedPreferencesUtil.getInstance().getBoolean(Constant.ISNIGHT)) {
            ChangeModeController.changeNight(SettingActivity.this, R.style.NightTheme);
        } else {
            ChangeModeController.changeNight(SettingActivity.this, R.style.DayTheme);
            initTheme();
        }
    }

    private void initTheme() {
        int color = SettingManager.getInstance().getThemeColor();
        statusBarView = StatusBarCompat.compat(this, color);
        toolbar.setBackgroundColor(color);
    }

    public void initToolBar() {
        toolbar.setTitle("设置");
        toolbar.setNavigationIcon(R.drawable.ab_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void initDatas() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                final String cachesize = CacheManager.getInstance().getCacheSize();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mTvCacheSize.setText(cachesize);
                    }
                });

            }
        }).start();
        bookrackSortWay = new String[]{getString(R.string.setting_dialog_sort_time), getString(R.string.setting_dialog_sort_recent)};
        readStyle = new String[]{getString(R.string.setting_dialog_style_raise), getString(R.string.setting_dialog_style_scroll),
                getString(R.string.setting_dialog_style_noeffect)};
        mTvSort.setText(bookrackSortWay[SharedPreferencesUtil.getInstance().getBoolean(Constant.ISBYUPDATESORT, true) ? 0 : 1]);
        mTvFlipStyle.setText(readStyle[SharedPreferencesUtil.getInstance().getInt(Constant.FLIP_STYLE, 0)]);
    }

    @OnClick(R.id.bookshelfSort)
    public void onClickBookShelfSort() {
        new AlertDialog.Builder(mContext)
                .setTitle("书架排序方式")
                .setSingleChoiceItems(bookrackSortWay,
                        SharedPreferencesUtil.getInstance().getBoolean(Constant.ISBYUPDATESORT, true) ? 0 : 1,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                mTvSort.setText(bookrackSortWay[which]);
                                SharedPreferencesUtil.getInstance().putBoolean(Constant.ISBYUPDATESORT, which == 0);
                                //TODO 通知书架进行数据的更新

                                dialog.dismiss();
                            }
                        })
                .create().show();
    }

    @OnClick(R.id.rlFlipStyle)
    public void onClickFlipStyle() {
        new AlertDialog.Builder(mContext)
                .setTitle(getString(R.string.scroll_page_effect))
                .setSingleChoiceItems(readStyle,
                        SharedPreferencesUtil.getInstance().getInt(Constant.FLIP_STYLE, 0),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                mTvFlipStyle.setText(readStyle[which]);
                                SharedPreferencesUtil.getInstance().putInt(Constant.FLIP_STYLE, which);
                                dialog.dismiss();
                            }
                        })
                .create().show();
    }

    @OnClick(R.id.feedBack)
    public void feedBack() {
        Intent data = new Intent(Intent.ACTION_SENDTO);
        data.setData(Uri.parse("mailto:249674239@qq.com"));
        startActivity(data);
    }

    @OnClick(R.id.ali_pay)
    public void aliPay() {
        openAliPay2Pay(ALIPAY_PERSON);
    }

    @OnClick(R.id.cleanCache)
    public void onClickCleanCache() {
        //默认不勾选清空书架列表，防手抖！！
        final boolean selected[] = {true, false};
        new AlertDialog.Builder(mContext)
                .setTitle("清除缓存")
                .setCancelable(true)
                .setMultiChoiceItems(new String[]{"删除阅读记录", "清空书架列表"}, selected, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        selected[which] = isChecked;
                    }
                })
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                CacheManager.getInstance().clearCache(selected[0], selected[1]);
                                final String cacheSize = CacheManager.getInstance().getCacheSize();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mTvCacheSize.setText(cacheSize);
                                        //TODO 通知书架进行数据的更新

                                    }
                                });
                            }
                        }).start();
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create().show();
    }

    /**
     * 支付
     *
     * @param qrCode
     */
    private void openAliPay2Pay(String qrCode) {
        if (openAlipayPayPage(this, qrCode)) {
            Toast.makeText(this, "跳转成功", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "跳转失败", Toast.LENGTH_SHORT).show();
        }
    }

    public static boolean openAlipayPayPage(Context context, String qrcode) {
        try {
            qrcode = URLEncoder.encode(qrcode, "utf-8");
        } catch (Exception e) {
        }
        try {
            final String alipayqr = "alipayqr://platformapi/startapp?saId=10000007&clientVersion=3.7.0.0718&qrcode=" + qrcode;
            openUri(context, alipayqr + "%3F_s%3Dweb-other&_t=" + System.currentTimeMillis());
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 发送一个intent
     *
     * @param context
     * @param s
     */
    private static void openUri(Context context, String s) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(s));
        context.startActivity(intent);
    }
}
