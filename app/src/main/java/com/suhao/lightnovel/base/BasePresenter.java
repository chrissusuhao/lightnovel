package com.suhao.lightnovel.base;

/**
 * BasePresenter
 */

public interface BasePresenter {
    void subscribe();
    void unSubscribe();
}
