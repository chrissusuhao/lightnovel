package com.suhao.lightnovel.base;

import com.suhao.lightnovel.MyApplication;
import com.suhao.lightnovel.utils.FileUtil;

/**
 * Created by bz on 2018/4/14.
 */

public class Constant {
    public static final String HIRAGANA_TRANSLATE = "http://trans.hiragana.jp/ruby/";

    public static String PATH_DATA = FileUtil.createRootPath(MyApplication.getInstance());

    public static String PATH_TXT = PATH_DATA + "/book/";

    public static final String ISNIGHT = "isNight";
    public static final String FLIP_STYLE = "flipStyle";
    public static String BASE_PATH = MyApplication.getInstance().getCacheDir().getPath();
    public static final String ISBYUPDATESORT = "isByUpdateSort";

}
