package com.suhao.lightnovel.base;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.suhao.lightnovel.utils.MobileInfoUtil;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by bz on 2018/4/18.
 */

public abstract class BaseDialogFragment extends DialogFragment {
    private Unbinder unbinder;
    /**
     * 获取布局ID
     */
    protected abstract int getContentViewLayoutID();

    /**
     * 界面初始化
     */
    protected abstract void init();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getContentViewLayoutID() != 0) {
            return inflater.inflate(getContentViewLayoutID(), container, false);
        } else {
            return super.onCreateView(inflater, container, savedInstanceState);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        init();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        System.gc();
    }

    protected void setWidthHeight(int w, int h) {
        Window window = getDialog().getWindow();

        WindowManager.LayoutParams attributes = window.getAttributes();
        attributes.width = MobileInfoUtil.dp2px(this.getActivity(), w);
        attributes.height = MobileInfoUtil.dp2px(this.getActivity(), h);
        window.setAttributes(attributes);
    }
}
