/**
 * Copyright 2016 JustWayward Team
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.suhao.lightnovel.ui.view.readview;

import android.app.Instrumentation;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.AudioManager;
import android.os.SystemClock;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ProgressBar;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.suhao.lightnovel.R;
import com.suhao.lightnovel.entity.ChapterEntity;
import com.suhao.lightnovel.manager.CacheManager;
import com.suhao.lightnovel.manager.SettingManager;
import com.suhao.lightnovel.utils.FileUtil;
import com.suhao.lightnovel.utils.ScreenUtil;
import com.tencent.bugly.crashreport.CrashReport;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Stack;

public class PageFactory {
    private Context mContext;
    /**
     * 屏幕宽高
     */
    private int mHeight, mWidth;
    /**
     * 文字区域宽高
     */
    private int mVisibleHeight, mVisibleWidth;
    /**
     * 间距
     */
    private int marginHeight, marginWidth, lineDownDynamicSize = 14;
    /**
     * 字体大小
     */
    private int mFontSize, mHiraganaFontSize, mNumFontSize;
    /**
     * 每页行数
     */
    private int mPageLineCount;
    /**
     * 行间距
     **/
    private int mLineSpace;
    /**
     * 页首页尾的位置
     */
    private int curEndPos = 0, curBeginPos = 0, tempBeginPos, tempEndPos;
    private int currentChapter, tempChapter;
    private List<String> mKanji;
    private List<String> mHiragana;

    private Paint mHiraganaPaint;
    private Paint mKanjiPaint;
    private Paint mTitlePaint;
    private Bitmap mBookPageBg;

    private DecimalFormat decimalFormat = new DecimalFormat("#0.00");
    private SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
    private int timeLen = 0, percentLen = 0;
    private String time;
    private int battery = 40;
    private Rect rectF;
    private ProgressBar batteryView;
    private Bitmap batteryBitmap;

    private String bookId;
    private List<ChapterEntity> chaptersList;
    private int chapterSize = 0;
    private int currentPage = 1;

    private OnReadStateChangeListener listener;
    private String charset = "UTF-8";

    public PageFactory(Context context, String bookId, List<ChapterEntity> chaptersList) {
        this(context, ScreenUtil.getScreenWidth(), ScreenUtil.getScreenHeight(),
                SettingManager.getInstance().getReadFontSize(), SettingManager.getInstance().getHiraganaFontSize(), bookId, chaptersList);
    }

    public PageFactory(Context context, int width, int height, int fontSize, int hiraganaFontSize, String bookId,
                       List<ChapterEntity> chaptersList) {
        mContext = context;
        mWidth = width;
        mHeight = height;
        mFontSize = fontSize;
        mHiraganaFontSize = hiraganaFontSize;
        mLineSpace = mFontSize / 5 * 2;
        mNumFontSize = ScreenUtil.dpToPxInt(16);
        marginWidth = ScreenUtil.dpToPxInt(15);
        marginHeight = ScreenUtil.dpToPxInt(15);
        mVisibleHeight = mHeight - marginHeight * 2 - mNumFontSize * 2 - mLineSpace * 2;
        mVisibleWidth = mWidth - marginWidth * 2 - 20;
        mPageLineCount = mVisibleHeight / (mFontSize + mHiraganaFontSize + mLineSpace);
        rectF = new Rect(0, 0, mWidth, mHeight);

        mHiraganaPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mHiraganaPaint.setTextSize(mHiraganaFontSize);
        mHiraganaPaint.setTextSize(ContextCompat.getColor(context, R.color.chapter_content_day));
        mHiraganaPaint.setColor(Color.BLACK);

        mKanjiPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mKanjiPaint.setTextSize(mFontSize);
        mKanjiPaint.setTextSize(ContextCompat.getColor(context, R.color.chapter_content_day));
        mKanjiPaint.setColor(Color.BLACK);

        mTitlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mTitlePaint.setTextSize(mNumFontSize);
        mTitlePaint.setColor(ContextCompat.getColor(context, R.color.chapter_title_day));
        timeLen = (int) mTitlePaint.measureText("00:00");
        percentLen = (int) mTitlePaint.measureText("00.00%");
        // Typeface typeface = Typeface.createFromAsset(context.getAssets(),"fonts/FZBYSK.TTF");
        // mKanjiPaint.setTypeface(typeface);
        // mNumPaint.setTypeface(typeface);

        this.bookId = bookId;
        this.chaptersList = chaptersList;

        time = dateFormat.format(new Date());
    }

    public void openBook(int indexPage) {
        openBook(1, indexPage);
    }

    /**
     * 打开书籍文件
     *
     * @param chapter  阅读章节
     * @param indexPage 阅读位置
     * @return 0：文件不存在或打开失败  1：打开成功
     */
    public int openBook(int chapter, int indexPage) {
        LogUtils.e("openBook");
        currentChapter = chapter;
        chapterSize = chaptersList.size();
        currentPage = indexPage;
        if (currentChapter > chapterSize) currentChapter = chapterSize;

        mKanji = FileUtil.chapterString2ArrayList(bookId, chapter);
        mHiragana = FileUtil.chapterString2ArrayList(bookId, chapter + CacheManager.HIRAGANA_SUFFIX);
        handleEverything();
        LogUtils.i(indexEachPage);
        LogUtils.i(lineDownDrawEachPage);
        LogUtils.i(startXEachPage);
        if (currentPage == -1) currentPage = indexEachPage.size();
        onChapterChanged(chapter);
        if (mKanji != null && mKanji.size() > 10 && mHiragana != null && mHiragana.size() > 10) return 1;
        return 0;
    }

    private void handleEverything() {
        indexEachPage.clear();
        lineDownDrawEachPage.clear();
        startXEachPage.clear();
        indexNextPage = 0;
        try {
            while (indexNextPage != mKanji.size() - 1) calculateEverything();
        } catch (Exception e) {
            CrashReport.postCatchedException(e);
        }
    }

    private Stack<Integer> indexEachPage = new Stack<>();
    private Stack<String> lineDownDrawEachPage = new Stack<>();
    private Stack<Integer> startXEachPage = new Stack<>();
    private int indexNextPage;
    private String lineDownDrawAtNextPage;
    private int startXNextPage; //汉字太长被断到下一页的时候需要重新设置开始绘制X坐标
    private int kanjiTuning = 0;

    private void calculateEverything() {
        if (mKanji != null && mKanji.size() > 0) {
            int y = marginHeight + (mLineSpace << 1);
            int initX = marginWidth;
            int initY = y + mLineSpace + mNumFontSize;
            int startX = initX; // 开始绘制的x起点
            int startY = initY; // 开始绘制的y起点
            int widthPadding = 5;

            Rect kanjiRect = new Rect(), hiraganaRect = new Rect(), lineDownOverflowRect = new Rect();
            String kanji, hiragana;
            int combineWidth, kanjiWidth, hiraganaWidth;
            int hanZiCenterPadding, pinYinCenterPadding;
            indexEachPage.push(indexNextPage);
            lineDownDrawEachPage.push(lineDownDrawAtNextPage);
            startXEachPage.push(startXNextPage);
            for (int i = indexNextPage; i < mKanji.size(); i++) {
                if (i == mKanji.size() - 1) indexNextPage = mKanji.size() - 1;
                // 拿到hanzi，与宽高
                kanji = mKanji.get(i);
                mKanjiPaint.getTextBounds(kanji, 0, kanji.length(), kanjiRect);
                kanjiWidth = kanjiRect.right - kanjiRect.left + kanjiTuning;
                // 拿到拼音，与宽高
                hiragana = mHiragana.get(i);
                mHiraganaPaint.getTextBounds(hiragana, 0, hiragana.length(), hiraganaRect);
                hiraganaWidth = hiraganaRect.right - hiraganaRect.left;
                // 比较 拼音 - 汉字 的宽度，取最宽的为标准
                combineWidth = Math.max(hiraganaWidth, kanjiWidth);

                // 居中显示
                hanZiCenterPadding = (combineWidth - kanjiWidth) / 2;
                pinYinCenterPadding = (combineWidth - hiraganaWidth) / 2;

                //如果是下一页且lineDownDrawAtNextPage有内容的话就绘制
                if (!TextUtils.isEmpty(lineDownDrawAtNextPage)) {
                    startX = startXNextPage;
                    startXNextPage = 0;
                    lineDownDrawAtNextPage = null;
                }

                // 换行
                if (startX + hiraganaWidth >= mVisibleWidth) {
                    startX = initX;
                    startY = startY + (mFontSize + mHiraganaFontSize + mLineSpace);
                } else if (startX + kanjiWidth >= mVisibleWidth) {
                    int paintSize = (int) (((mVisibleWidth - startX) / (float)kanjiWidth) * kanji.length()) - 2;
                    paintSize = paintSize < 0 ? 0 : paintSize;
                    startY = startY + (mFontSize + mHiraganaFontSize + mLineSpace);
                    String lineDown = kanji.substring(paintSize, kanji.length());
                    if (startY > mVisibleHeight) {
                        indexNextPage = i;
                        lineDownDrawAtNextPage = lineDown;
                        startXNextPage = initX + hanZiCenterPadding + startX + kanjiWidth - mVisibleWidth + lineDown.length() * lineDownDynamicSize;
                        LogUtils.e("汉字太长被断开时的indexNextPage：" + indexNextPage);
                        break;
                    }

                    mKanjiPaint.getTextBounds(lineDown, 0, lineDown.length(), lineDownOverflowRect);
                    int lineDownOverflowWidth = lineDownOverflowRect.right - lineDownOverflowRect.left;
                    if (lineDownOverflowWidth > mVisibleWidth) {
                        int lineRepeatCount = lineDownOverflowWidth / mVisibleWidth;

                        for (int repeatDraw = 0; repeatDraw < lineRepeatCount; repeatDraw++) {
                            int size = (int) (((mVisibleWidth) / (float) lineDownOverflowWidth) * lineDown.length()) - 2;
                            size = size < 0 ? 0 : size;
                            lineDown = lineDown.substring(size, lineDown.length());
                            startY = startY + (mFontSize + mHiraganaFontSize + mLineSpace);
                        }
                    }

                    startX = initX + hanZiCenterPadding + startX + kanjiWidth - mVisibleWidth + lineDown.length() * lineDownDynamicSize;
                    continue;
                }


                if (startY > mVisibleHeight) {
                    indexNextPage = i;
                    LogUtils.e("回车换行时的indexNextPage：" + indexNextPage);
                    break;
                } else {
                    startX = startX + combineWidth + widthPadding;
                }


                if (kanji.contains("\n") || kanji.contains("\r\n")) {
                    startX = initX;
                    startY = startY + (mFontSize + mHiraganaFontSize + 4 * mLineSpace);
                }
            }
        }
    }

    private synchronized void onDraw(Canvas canvas, int indexPage) {
        if (mKanji != null && mKanji.size() > 0) {
            int y = marginHeight + (mLineSpace << 1);
            // 绘制背景
            if (mBookPageBg != null) {
                canvas.drawBitmap(mBookPageBg, null, rectF, null);
            } else {
                canvas.drawColor(Color.WHITE);
            }
            // 绘制标题
            canvas.drawText(chaptersList.get(currentChapter - 1).getTitle(), marginWidth, y, mTitlePaint);
            // 绘制阅读页面文字
            int initX = marginWidth;
            int initY = y + mLineSpace + mNumFontSize;
            int startX = initX; // 开始绘制的x起点
            int startY = initY; // 开始绘制的y起点
            int widthPadding = 5;

            Rect kanjiRect = new Rect(), hiraganaRect = new Rect(), lineDownOverflowRect = new Rect();
            String kanji, hiragana;
            int combineWidth, kanjiWidth, hiraganaWidth;
            int hanZiCenterPadding, pinYinCenterPadding;
            String lineDownDraw = lineDownDrawEachPage.get(indexPage - 1);
            String lineDownDrawNextPage = indexPage == lineDownDrawEachPage.size() ? null : lineDownDrawEachPage.get(indexPage);
            int tmpBeginIndex, tmpEndIndex;
            tmpBeginIndex = indexEachPage.get(indexPage - 1);
            tmpEndIndex = indexPage == indexEachPage.size() ? mKanji.size() : indexEachPage.get(indexPage);
            if (!TextUtils.isEmpty(lineDownDraw)){
                tmpBeginIndex = indexEachPage.get(indexPage - 1) + 1;
            }
            if (!TextUtils.isEmpty(lineDownDrawNextPage)) {
                tmpEndIndex = indexPage == indexEachPage.size() ? mKanji.size() : indexEachPage.get(indexPage) + 1;
            }

            LogUtils.e("tmpBeginIndex " + tmpBeginIndex + " tmpEndIndex " + tmpEndIndex);
            for (int i = tmpBeginIndex; i < tmpEndIndex; i++) {
                // 拿到hanzi，与宽高
                kanji = mKanji.get(i);
                mKanjiPaint.getTextBounds(kanji, 0, kanji.length(), kanjiRect);
                kanjiWidth = kanjiRect.right - kanjiRect.left + kanjiTuning;
                // 拿到拼音，与宽高
                hiragana = mHiragana.get(i);
                mHiraganaPaint.getTextBounds(hiragana, 0, hiragana.length(), hiraganaRect);
                hiraganaWidth = hiraganaRect.right - hiraganaRect.left;
                // 比较 拼音 - 汉字 的宽度，取最宽的为标准
                combineWidth = Math.max(hiraganaWidth, kanjiWidth);

                // 居中显示
                hanZiCenterPadding = (combineWidth - kanjiWidth) / 2;
                pinYinCenterPadding = (combineWidth - hiraganaWidth) / 2;

                //如果是下一页且lineDownDrawAtNextPage有内容的话就绘制
                if (!TextUtils.isEmpty(lineDownDraw)) {
                    canvas.drawText("", initX, startY, mHiraganaPaint);
                    canvas.drawText(lineDownDraw, initX, startY + (mFontSize + mHiraganaFontSize + mLineSpace) / 2, mKanjiPaint);
                    startX = startXEachPage.get(indexPage - 1);
                    lineDownDraw = null;
                }

                // 换行
                if (startX + hiraganaWidth >= mVisibleWidth) {
                    startX = initX;
                    startY = startY + (mFontSize + mHiraganaFontSize + mLineSpace);
                } else if (startX + kanjiWidth >= mVisibleWidth) {
                    int paintSize = (int) (((mVisibleWidth - startX) / (float) kanjiWidth) * kanji.length()) - 2;
                    paintSize = paintSize < 0 ? 0 : paintSize;
                    String lineUp = kanji.substring(0, paintSize);
                    canvas.drawText("", startX + pinYinCenterPadding, startY, mHiraganaPaint);
                    canvas.drawText(lineUp, startX + hanZiCenterPadding, startY + (mFontSize + mHiraganaFontSize + mLineSpace) / 2, mKanjiPaint);

                    startY = startY + (mFontSize + mHiraganaFontSize + mLineSpace);

                    String lineDown = kanji.substring(paintSize, kanji.length());

                    if (startY > mVisibleHeight) {
                        break;
                    }

                    mKanjiPaint.getTextBounds(lineDown, 0, lineDown.length(), lineDownOverflowRect);
                    int lineDownOverflowWidth = lineDownOverflowRect.right - lineDownOverflowRect.left;
                    if (lineDownOverflowWidth > mVisibleWidth) {
                        int lineRepeatCount = lineDownOverflowWidth / mVisibleWidth;

                        for (int repeatDraw = 0; repeatDraw < lineRepeatCount; repeatDraw++) {
                            int size = (int) (((mVisibleWidth) / (float) lineDownOverflowWidth) * lineDown.length()) - 2;
                            size = size < 0 ? 0 : size;
                            String lineRepeat = lineDown.substring(0, size);
                            lineDown = lineDown.substring(size, lineDown.length());
                            canvas.drawText("", initX + pinYinCenterPadding, startY, mHiraganaPaint);
                            canvas.drawText(lineRepeat, initX + hanZiCenterPadding, startY + (mFontSize + mHiraganaFontSize + mLineSpace) / 2, mKanjiPaint);
                            startY = startY + (mFontSize + mHiraganaFontSize + mLineSpace);
                        }
                    }

                    canvas.drawText(hiragana, initX + pinYinCenterPadding, startY, mHiraganaPaint);
                    canvas.drawText(lineDown, initX + hanZiCenterPadding, startY + (mFontSize + mHiraganaFontSize + mLineSpace) / 2, mKanjiPaint);

                    startX = initX + hanZiCenterPadding + startX + kanjiWidth - mVisibleWidth + lineDown.length() * lineDownDynamicSize;

                    continue;
                }


                if (startY > mVisibleHeight) {
                    break;
                } else {
                    // startY 是 baseLine
                    canvas.drawText(hiragana, startX + pinYinCenterPadding, startY, mHiraganaPaint);
                    canvas.drawText(kanji, startX + hanZiCenterPadding, startY + (mFontSize + mHiraganaFontSize + mLineSpace) / 2, mKanjiPaint);
                    // 下一个绘制的x
                    startX = startX + combineWidth + widthPadding;
                }

                if (kanji.contains("\n") || kanji.contains("\r\n")) {
                    startX = initX;
                    startY = startY + (mFontSize + mHiraganaFontSize + 4 * mLineSpace);
                }
            }
            drawPromptContent(canvas);
        }
        //保存阅读进度
        SettingManager.getInstance().saveReadProgress(bookId, currentChapter, currentPage);
    }

    /**
     * 绘制阅读页面
     *
     * @param canvas
     */
    synchronized void onDraw(Canvas canvas) {
        if (currentPage > indexEachPage.size()) currentPage = indexEachPage.size();
        else if (currentPage == -1) currentPage = 1;
        onDraw(canvas, currentPage);
    }

    private void drawPromptContent(Canvas canvas) {
        // 绘制提示内容
        if (batteryBitmap != null) {
            canvas.drawBitmap(batteryBitmap, marginWidth + 2,
                    mHeight - marginHeight - ScreenUtil.dpToPxInt(12), mTitlePaint);
        }

        float percent = (float) currentChapter * 100 / chapterSize;
        canvas.drawText(decimalFormat.format(percent) + "%", (mWidth - percentLen) / 2,
                mHeight - marginHeight, mTitlePaint);

        String mTime = dateFormat.format(new Date());
        canvas.drawText(mTime, mWidth - marginWidth - timeLen, mHeight - marginHeight, mTitlePaint);
    }

    private boolean hasNextPage() {
        return currentChapter < chaptersList.size() || currentPage < indexEachPage.size();
    }

    private boolean hasPrePage() {
        return currentChapter > 1 || (currentChapter == 1 && currentPage > 1);
    }

    /**
     * 跳转下一页
     */
    BookStatus nextPage() {
        if (!hasNextPage()) { // 最后一章的结束页
            return BookStatus.NO_NEXT_PAGE;
        } else {
            if (currentPage >= indexEachPage.size()) {
                currentChapter++;
                int ret = openBook(currentChapter, 1);
                if (ret == 0) {
                    onLoadChapterFailure(currentChapter);
                    currentChapter--;
                    return BookStatus.NEXT_CHAPTER_LOAD_FAILURE;
                } else {
                    currentPage = 0;
                    onChapterChanged(currentChapter);
                }
            }
            onPageChanged(currentChapter, ++currentPage);
        }
        return BookStatus.LOAD_SUCCESS;
    }

    /**
     * 跳转上一页
     */
    BookStatus prePage() {
        if (!hasPrePage()) { // 第一章第一页
            return BookStatus.NO_PRE_PAGE;
        } else {
            // 保存当前页的值
            if (currentPage == 1) {
                currentChapter--;
                int ret = openBook(currentChapter, -1);
                if (ret == 0) {
                    onLoadChapterFailure(currentChapter);
                    currentChapter++;
                    return BookStatus.PRE_CHAPTER_LOAD_FAILURE;
                } else { // 跳转到上一章的最后一页
                    onChapterChanged(currentChapter);
                    onPageChanged(currentChapter, currentPage);
                    return BookStatus.LOAD_SUCCESS;
                }
            }
            onPageChanged(currentChapter, --currentPage);
        }
        return BookStatus.LOAD_SUCCESS;
    }

    void cancelPage() {
    }

    /**
     * 获取当前阅读位置
     *
     * @return page 0：起始位置 1：结束位置
     */
    public int[] getPosition() {
        return new int[]{currentChapter, currentPage};
    }

    public String getHeadLineStr() {
        return mKanji.get(indexEachPage.peek());
    }

    /**
     * 设置字体大小
     *
     * @param fontsize 单位：px
     */
    public void setTextFont(int fontsize) {
        LogUtils.i("fontSize=" + fontsize);
        mFontSize = fontsize;
        mLineSpace = mFontSize / 5 * 2;
        mKanjiPaint.setTextSize(mFontSize);
        mHiraganaPaint.setTextSize(mFontSize);
        mPageLineCount = mVisibleHeight / (mFontSize + mLineSpace);
        handleEverything();
    }

    public void setHiraganaTextFont(int fontsize) {
        LogUtils.i("hiraganaFontSize=" + fontsize);
        mHiraganaFontSize = fontsize;
        mHiraganaPaint.setTextSize(mHiraganaFontSize);
        handleEverything();
    }

    /**
     * 设置字体颜色
     *
     * @param textColor
     * @param titleColor
     */
    public void setTextColor(int textColor, int titleColor) {
        mKanjiPaint.setColor(textColor);
        mHiraganaPaint.setColor(textColor);
        mTitlePaint.setColor(titleColor);
    }

    public int getTextFont() {
        return mFontSize;
    }


    public void setBgBitmap(Bitmap BG) {
        mBookPageBg = BG;
    }

    public void setOnReadStateChangeListener(OnReadStateChangeListener listener) {
        this.listener = listener;
    }

    private void onChapterChanged(int chapter) {
        if (listener != null) {
            listener.onChapterChanged(chapter);
        }
    }

    private void onPageChanged(int chapter, int page) {
        if (listener != null)
            listener.onPageChanged(chapter, page);
    }

    private void onLoadChapterFailure(int chapter) {
        if (listener != null)
            listener.onLoadChapterFailure(chapter);
    }

    public void convertBetteryBitmap() {
        batteryView = (ProgressBar) LayoutInflater.from(mContext).inflate(R.layout.layout_battery_progress, null);
        batteryView.setProgressDrawable(ContextCompat.getDrawable(mContext,
                SettingManager.getInstance().getReadTheme() < 4 ?
                        R.drawable.seekbar_battery_bg : R.drawable.seekbar_battery_night_bg));
        batteryView.setProgress(battery);
        batteryView.setDrawingCacheEnabled(true);
        batteryView.measure(View.MeasureSpec.makeMeasureSpec(ScreenUtil.dpToPxInt(26), View.MeasureSpec.EXACTLY),
                View.MeasureSpec.makeMeasureSpec(ScreenUtil.dpToPxInt(14), View.MeasureSpec.EXACTLY));
        batteryView.layout(0, 0, batteryView.getMeasuredWidth(), batteryView.getMeasuredHeight());
        batteryView.buildDrawingCache();
        //batteryBitmap = batteryView.getDrawingCache();
        // tips: @link{https://github.com/JustWayward/BookReader/issues/109}
        batteryBitmap = Bitmap.createBitmap(batteryView.getDrawingCache());
        batteryView.setDrawingCacheEnabled(false);
        batteryView.destroyDrawingCache();
    }

    public void setBattery(int battery) {
        this.battery = battery;
        convertBetteryBitmap();
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void recycle() {
        if (mBookPageBg != null && !mBookPageBg.isRecycled()) {
            mBookPageBg.recycle();
            mBookPageBg = null;
            LogUtils.d("mBookPageBg recycle");
        }

        if (batteryBitmap != null && !batteryBitmap.isRecycled()) {
            batteryBitmap.recycle();
            batteryBitmap = null;
            LogUtils.d("batteryBitmap recycle");
        }
    }

    private boolean isTtsInitialized = false;
    private int langTTSavailable = -1;
    private TextToSpeech mTts;

    public void ttsPlay() {
        if (!isTtsInitialized) {
            ToastUtils.showLong(mContext.getString(R.string.engine_initializing));
            mTts = new TextToSpeech(mContext, new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    if (status == TextToSpeech.SUCCESS) {
                        langTTSavailable = mTts.setLanguage(Locale.JAPAN);
                        if (langTTSavailable == TextToSpeech.LANG_MISSING_DATA ||
                                langTTSavailable == TextToSpeech.LANG_NOT_SUPPORTED) {
                            isTtsInitialized = false;
                            showTTSNotSupportDialog();
                        } else if ( langTTSavailable >= 0) {
                            isTtsInitialized = true;
                            myHashAlarm = new HashMap<>();
                            inst = new Instrumentation();
                            myHashAlarm.put(TextToSpeech.Engine.KEY_PARAM_STREAM, String.valueOf(AudioManager.STREAM_MUSIC));
                            myHashAlarm.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "nextPage");
                            mTts.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                                @Override
                                public void onStart(String utteranceId) {
                                    LogUtils.e("onStart utteranceId---" + utteranceId);
                                }

                                @Override
                                public void onDone(String utteranceId) {
                                    LogUtils.e("onDone utteranceId---" + utteranceId);
                                    inst.sendPointerSync(MotionEvent.obtain(SystemClock.uptimeMillis(),
                                            SystemClock.uptimeMillis(),
                                            MotionEvent.ACTION_DOWN, ScreenUtil.getScreenWidth() - 100,
                                            ScreenUtil.getScreenHeight() - 100, 0));
                                    inst.sendPointerSync(MotionEvent.obtain(SystemClock.uptimeMillis(),
                                            SystemClock.uptimeMillis(),
                                            MotionEvent.ACTION_UP, ScreenUtil.getScreenWidth() - 100,
                                            ScreenUtil.getScreenHeight() - 100, 0));
                                }

                                @Override
                                public void onError(String utteranceId) {
                                    LogUtils.e("onError utteranceId---" + utteranceId);
                                }
                            });
                            ttsSpeak();
                        }
                    } else {
                        ToastUtils.showShort("初始化失败未知原因");
                        showTTSNotSupportDialog();
                        isTtsInitialized = false;
                    }
                }
            });
        } else {
            ttsSpeak();
        }
    }

    private void showTTSNotSupportDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(mContext.getString(R.string.tts_can_not_work))
                .setCancelable(true)
                .setMessage(R.string.add_tts_language)
                .setPositiveButton("确定", null);

        builder.create().show();
    }

    private Instrumentation inst;
    private HashMap<String, String> myHashAlarm;

    public void ttsSpeak() {
        String readString = buildReadString();
        mTts.speak(readString, TextToSpeech.QUEUE_FLUSH, myHashAlarm);
    }

    private String buildReadString() {
        StringBuilder readSB = new StringBuilder();
        int tmpBeginIndex, tmpEndIndex;
        tmpBeginIndex = indexEachPage.get(currentPage - 1);
        tmpEndIndex = currentPage == indexEachPage.size() ? mKanji.size() : indexEachPage.get(currentPage);
        for (int i = tmpBeginIndex; i < tmpEndIndex; i++) {
            if (!TextUtils.isEmpty(mHiragana.get(i)) && !TextUtils.equals(mHiragana.get(i), " ")) {
                readSB.append(mHiragana.get(i));
            } else {
                readSB.append(mKanji.get(i));
            }
        }
        return readSB.toString();
    }

    public void ttsStop() {
        if (mTts != null) {
            isTtsInitialized = false;
            mTts.stop();
            mTts.shutdown();
        }
    }

    public boolean isTTSWorking() {
        if (mTts == null) return false;
        return isTtsInitialized;
    }

}
