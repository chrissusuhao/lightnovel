package com.suhao.lightnovel.ui.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.widget.TextView;

import com.blankj.utilcode.util.LogUtils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@SuppressLint("AppCompatCustomView")
public class HiraganaTextView extends TextView {

    private List<String> kanji;
    private List<String> hiragana;
    private Set<Integer> newLineIndex = new HashSet<>(Arrays.asList(7, 15, 23, 31, 39, 47, 55, 63, 71, 79, 87));
    private float height;
    private TextPaint kanjiPaint = new TextPaint();
    private TextPaint hiraganaPaint = new TextPaint();
    private int viewWidth;
    private int viewHeight;

    public HiraganaTextView(Context context) {
        this(context, null);
    }

    public HiraganaTextView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HiraganaTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initPaint();
    }

    private int lineHeight = 60;
    private int linePadding = 5;
    private int baseLine = 30;
    private float kanjiTextSize = 16;
    private float hiraganaTextSize = 12;

    private void initPaint() {
        kanjiPaint.setAntiAlias(true);
        kanjiPaint.setTextSize(kanjiTextSize * getResources().getDisplayMetrics().density);
        kanjiPaint.setColor(0xFF000000);
        Paint.FontMetricsInt fontMetrics = kanjiPaint.getFontMetricsInt();
        int baseLine = Math.abs(fontMetrics.top);

        hiraganaPaint.setAntiAlias(true);
        hiraganaPaint.setTextSize(hiraganaTextSize * getResources().getDisplayMetrics().density);
        hiraganaPaint.setColor(0xFF000000);
        Paint.FontMetricsInt fontMetricsHiragana = kanjiPaint.getFontMetricsInt();
        int baseLineHiragana = Math.abs(fontMetricsHiragana.top);

        lineHeight = baseLine + baseLineHiragana - 10;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int desiredWidth = 100;
        int desiredHeight = 100;

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int width;
        int height;

        //Measure Width
        if (widthMode == MeasureSpec.EXACTLY) {
            //Must be this size
            width = widthSize;
        } else if (widthMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            width = Math.min(desiredWidth, widthSize);
        } else {
            //Be whatever you want
            width = desiredWidth;
        }

        //Measure Height
        if (heightMode == MeasureSpec.EXACTLY) {
            //Must be this size
            height = heightSize;
        } else if (heightMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            height = (lineHeight + linePadding) * newLineIndex.size();
        } else {
            //Be whatever you want
            height = (lineHeight + linePadding) * newLineIndex.size();
        }

        viewWidth = width;
        viewHeight = height;

        //MUST CALL THIS
        setMeasuredDimension(width, height * 5);

    }

    public void setKanji(List kanji) {
        this.kanji = kanji;
    }

    public void setHiragana(List hiragana) {
        this.hiragana = hiragana;
    }

    public void setNewLineIndex(Set<Integer> newLineIndex) {
        this.newLineIndex = newLineIndex;
        setHeight((lineHeight + linePadding) * newLineIndex.size());
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        kanjiPaint.setColor(Color.BLACK);
        drawText(canvas);
    }

    private void drawText(Canvas canvas) {
        int initX = getPaddingLeft();
        int initY = getPaddingTop() + baseLine;
        int startX = initX; // 开始绘制的x起点
        int startY = initY; // 开始绘制的y起点
        int widthPadding = 5;

        Rect kanjiRect, hiraganaRect;
        String kanji, hiragana;
        int combineWidth, kanjiWidth, hiraganaWidth;
        int hanZiCenterPadding, pinYinCenterPadding;
        for (int i = 0; i < this.kanji.size(); i++) {
            // 拿到hanzi，与宽高
            kanji = this.kanji.get(i);
            kanjiRect = new Rect();
            kanjiPaint.getTextBounds(kanji, 0, kanji.length(), kanjiRect);
            kanjiWidth = kanjiRect.right - kanjiRect.left;
            // 拿到拼音，与宽高
            hiragana = this.hiragana.get(i);
            hiraganaRect = new Rect();
            hiraganaPaint.getTextBounds(hiragana, 0, hiragana.length(), hiraganaRect);
            hiraganaWidth = hiraganaRect.right - hiraganaRect.left;
            // 比较 拼音 - 汉字 的宽度，取最宽的为标准
            combineWidth = Math.max(hiraganaWidth, kanjiWidth);

            // 居中显示
            hanZiCenterPadding = (combineWidth - kanjiWidth) / 2;
            pinYinCenterPadding = (combineWidth - hiraganaWidth) / 2;

            // 换行 无需绘制
            if (startX + hiraganaWidth >= viewWidth || startX + kanjiWidth >= viewWidth) {
                startX = initX;
                startY = startY + lineHeight + linePadding;
            }
            // startY 是 baseLine
            canvas.drawText(hiragana, startX + pinYinCenterPadding, startY, hiraganaPaint);
            canvas.drawText(kanji, startX + hanZiCenterPadding, startY + lineHeight / 2, kanjiPaint);
            // 下一个绘制的x
            startX = startX + combineWidth + widthPadding;
        }
    }
}
