package com.suhao.lightnovel.ui.view.dialog;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.suhao.lightnovel.R;
import com.suhao.lightnovel.base.BaseDialogFragment;
import com.wang.bookloading.widget.BookLoadingView;

import butterknife.BindView;

/**
 * Created by bz on 2018/4/18.
 */

public class BookLoadingDialog extends BaseDialogFragment {

    @BindView(R.id.book_loading_view)
    BookLoadingView mBookLoadingView;

    @Override
    protected int getContentViewLayoutID() {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return R.layout.dialog_book_loading;
    }

    @Override
    protected void init() {
        mBookLoadingView.setVisibility(View.VISIBLE, 0);
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = window.getAttributes();
        windowParams.dimAmount = 0.0f;
        window.setAttributes(windowParams);
        setWidthHeight(270, 221);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mBookLoadingView.setVisibility(View.GONE);
    }
}
