package com.suhao.lightnovel.ui.presenter;

import android.text.Html;

import com.blankj.utilcode.util.LogUtils;
import com.litesuits.orm.db.assit.QueryBuilder;
import com.suhao.lightnovel.MyApplication;
import com.suhao.lightnovel.R;
import com.suhao.lightnovel.base.RxPresenter;
import com.suhao.lightnovel.entity.CategoryEntity;
import com.suhao.lightnovel.entity.ChapterEntity;
import com.suhao.lightnovel.manager.CacheManager;
import com.suhao.lightnovel.ui.contract.NovelBookrackContract;
import com.suhao.lightnovel.ui.contract.NovelCategoryContract;
import com.suhao.lightnovel.utils.HtmlTagHandler;
import com.suhao.lightnovel.utils.JsoupUtil;
import com.suhao.lightnovel.utils.StringUtil;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by bz on 2018/4/18.
 */

public class NovelBookrackPresenter extends RxPresenter<NovelBookrackContract.View> implements NovelBookrackContract.Presenter<NovelBookrackContract.View> {

    @Inject
    NovelBookrackPresenter() {
    }

    @Override
    public void getNovels(final int type, final int page) {
        final List<CategoryEntity> categoryEntityList = new ArrayList<>();
        Disposable d = Observable.create(new ObservableOnSubscribe<Integer>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<Integer> step) {
                categoryEntityList.addAll(MyApplication.getInstance().getLiteOrm().query(new QueryBuilder<>(CategoryEntity.class)));
                LogUtils.i("categoryEntityList in database ", categoryEntityList);
                step.onNext(0);
                step.onComplete();
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .doOnNext(new Consumer<Integer>() {
                    @Override
                    public void accept(@NonNull Integer integer) throws Exception {
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Integer>() {
                    @Override
                    public void accept(@NonNull Integer integer) throws Exception {
                        if (mView != null) mView.showNovels(categoryEntityList);
                    }
                });
        register(d);
    }

    private boolean isFromNet = false;

    @Override
    public void getShortStory(final CategoryEntity categoryEntity) {
        final ArrayList<String> kanji = new ArrayList<>();
        final ArrayList<String> hiragana = new ArrayList<>();

        final Observable<String> cache = Observable.create(new ObservableOnSubscribe<String>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<String> step) throws Exception {
                LogUtils.e(categoryEntity.getUrl());
                boolean cached = CacheManager.getInstance().isChapterFileCached(categoryEntity.getBookId(), 1);
                if (cached) {
                    isFromNet = false;
                    step.onNext("");
                } else {
                    isFromNet = true;
                    step.onComplete();
                }
            }
        });

        Observable<String> network = Observable.create(new ObservableOnSubscribe<String>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<String> step) {
                LogUtils.d("Observable thread is : " + Thread.currentThread().getName());
                try {
                    Document doc = JsoupUtil.novelShortStoryConnector(categoryEntity.getUrl());
                    Elements elements = doc.select("div#novel_color").first().getElementsByAttributeValue("id", "novel_honbun");
                    Element element = elements.first();
//                    LogUtils.d(element);
                    HtmlTagHandler htmlTagHandler = new HtmlTagHandler(kanji, hiragana);
                    String html = htmlTagHandler.overrideTags(element.toString());
                    Html.fromHtml(html, null, htmlTagHandler);
                    step.onNext("");
                } catch (Exception e) {
                    LogUtils.e(e.toString());
                }
            }
        });

        Observable.concat(cache, network)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(@NonNull String step) throws Exception {
                        LogUtils.e("getShortStory isFromNet " + isFromNet);
                        if (isFromNet) {
                            CacheManager.getInstance().saveChapterFileWithHiragana(StringUtil.fromUrl2BookId(categoryEntity.getUrl()), 1, kanji, hiragana);
                        }
                        ChapterEntity.isShortStory = true;
                        ChapterEntity chapterEntity = new ChapterEntity();
                        chapterEntity.setBookId(categoryEntity.getBookId());
                        chapterEntity.setChapter(1);
                        chapterEntity.setTitle(categoryEntity.getTitle());
                        chapterEntity.setUrl(categoryEntity.getUrl());
                        List<ChapterEntity> chapterList = new ArrayList<>();
                        chapterList.add(chapterEntity);
                        if (mView != null) mView.gotoReadActivity(chapterList);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                        LogUtils.e(throwable.toString());
                    }
                });
    }

    List<ChapterEntity> chapterEntityList = new ArrayList<>();

    @Override
    public void getCatalogue(final CategoryEntity categoryEntity) {
        chapterEntityList.clear();
        ChapterEntity.isShortStory = false;
        final Observable<String> cache = Observable.create(new ObservableOnSubscribe<String>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<String> step) throws Exception {
                chapterEntityList = MyApplication.getInstance().getLiteOrm().query(
                        new QueryBuilder<>(ChapterEntity.class)
                                .whereEquals("book_id", categoryEntity.getBookId())
                                .appendOrderAscBy("chapter"));
//                LogUtils.i("chapterEntityList", chapterEntityList);
                boolean cached = !chapterEntityList.isEmpty();
                if (cached) {
                    isFromNet = false;
                    step.onNext("");
                } else {
                    isFromNet = true;
                    step.onComplete();
                }
            }
        });

        Observable<String> network = Observable.create(new ObservableOnSubscribe<String>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<String> step) {
                LogUtils.d("get chapterEntityList from net " + Thread.currentThread().getName());
                try {
                    Document doc = JsoupUtil.novelCatalogueConnector(categoryEntity.getUrl());
                    Element elementIndexBox = doc.select("div.index_box").first();
                    String part = "";
                    for (Element element : elementIndexBox.getAllElements()) {
                        if (element.hasClass("chapter_title")) {
                            part = element.getElementsByClass("chapter_title").first().text();
                        } else if (element.hasClass("novel_sublist2")) {
                            ChapterEntity chapterEntity = new ChapterEntity();
                            chapterEntity.setBookId(categoryEntity.getBookId());
                            Element elementSubtitle = element.getElementsByClass("subtitle").first();
                            String url = elementSubtitle.select("a").first().attr("abs:href");
                            chapterEntity.setChapter(StringUtil.fromUrl2Chapter(url));
                            chapterEntity.setPart(part);
                            chapterEntity.setTitle(elementSubtitle.select("a").first().text());
                            chapterEntity.setUrl(url);
                            chapterEntityList.add(chapterEntity);
                        }
                    }
//                    LogUtils.d(chapterEntityList);
                    step.onNext("");
                } catch (Exception e) {
                    LogUtils.e(e.toString());
                }
            }
        });

        Observable.concat(cache, network)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(@NonNull String step) throws Exception {
                        LogUtils.e("getCatalogue isFromNet " + isFromNet);
                        if (isFromNet) {
                            MyApplication.getInstance().getLiteOrm().save(chapterEntityList);
                        }
                        if (mView != null) mView.gotoReadActivity(chapterEntityList);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                        LogUtils.e(throwable.toString());
                    }
                });
    }
}
