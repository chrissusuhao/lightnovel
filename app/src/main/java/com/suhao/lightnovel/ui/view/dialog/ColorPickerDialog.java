package com.suhao.lightnovel.ui.view.dialog;

import android.widget.Button;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.larswerkman.holocolorpicker.ColorPicker;
import com.larswerkman.holocolorpicker.SVBar;
import com.suhao.lightnovel.R;
import com.suhao.lightnovel.activity.HomeActivity;
import com.suhao.lightnovel.base.BaseDialogFragment;
import com.suhao.lightnovel.manager.SettingManager;
import com.suhao.lightnovel.manager.communication.FunctionManager;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by bz on 2018/4/18.
 */

public class ColorPickerDialog extends BaseDialogFragment implements ColorPicker.OnColorChangedListener {

    @BindView(R.id.picker)
    ColorPicker colorPicker;
    @BindView(R.id.svbar)
    SVBar svBar;
    @BindView(R.id.button)
    Button button;

    private int mColor;

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.dialog_color_picker;
    }

    @Override
    protected void init() {
        colorPicker.addSVBar(svBar);
        colorPicker.setOnColorChangedListener(this);
        colorPicker.setOldCenterColor(SettingManager.getInstance().getThemeColor());
    }

    @Override
    public void onResume() {
        super.onResume();
        setWidthHeight(360, 400);
    }

    @Override
    public void onColorChanged(int color) {
        LogUtils.i("onColorChanged " + color);
        mColor = color;
        FunctionManager.getInstance().invokeFunction(HomeActivity.FUNC_THEME_COLOR_CHANGE, color);
    }

    @OnClick(R.id.button)
    public void onClick() {
        SettingManager.getInstance().saveThemeColor(mColor);
        dismiss();
        ToastUtils.showShort(R.string.color_theme_changed);
    }
}
