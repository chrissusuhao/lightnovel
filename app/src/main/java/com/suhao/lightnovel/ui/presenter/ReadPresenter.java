package com.suhao.lightnovel.ui.presenter;

import android.text.Html;

import com.blankj.utilcode.util.LogUtils;
import com.suhao.lightnovel.base.RxPresenter;
import com.suhao.lightnovel.entity.ChapterEntity;
import com.suhao.lightnovel.manager.CacheManager;
import com.suhao.lightnovel.ui.contract.BookReadContract;
import com.suhao.lightnovel.utils.HtmlTagHandler;
import com.suhao.lightnovel.utils.JsoupUtil;
import com.suhao.lightnovel.utils.StringUtil;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class ReadPresenter extends RxPresenter<BookReadContract.View> implements BookReadContract.Presenter<BookReadContract.View> {

    @Inject
    ReadPresenter() {
    }

    @Override
    public void getBookMixAToc(String bookId, String view) {

    }

    private boolean isFromNet;

    @Override
    public void getChapterRead(final ChapterEntity chapterEntity) {
        final ArrayList<String> kanji = new ArrayList<>();
        final ArrayList<String> hiragana = new ArrayList<>();

        final Observable<String> cache = Observable.create(new ObservableOnSubscribe<String>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<String> step) throws Exception {
                LogUtils.e(chapterEntity.getUrl());
                boolean cached = CacheManager.getInstance().isChapterFileCached(chapterEntity.getBookId(), chapterEntity.getChapter());
                if (cached) {
                    isFromNet = false;
                    step.onNext("");
                } else {
                    isFromNet = true;
                    step.onComplete();
                }
            }
        });

        Observable<String> network = Observable.create(new ObservableOnSubscribe<String>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<String> step) {
                LogUtils.d("Observable thread is : " + Thread.currentThread().getName());
                try {
                    Document doc = JsoupUtil.novelShortStoryConnector(chapterEntity.getUrl());
                    Elements elements = doc.select("div#novel_color").first().getElementsByAttributeValue("id", "novel_honbun");
                    Element element = elements.first();
//                    LogUtils.d(element);
                    HtmlTagHandler htmlTagHandler = new HtmlTagHandler(kanji, hiragana);
                    String html = htmlTagHandler.overrideTags(element.toString());
                    Html.fromHtml(html, null, htmlTagHandler);
                    step.onNext("");
                } catch (Exception e) {
                    if (mView != null) mView.netError(chapterEntity.getChapter());
                    LogUtils.e(e.toString());
                }
            }
        });

        Observable.concat(cache,network)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(@NonNull String step) throws Exception {
                        LogUtils.e("getChapterRead isFromNet " + isFromNet);
                        if (isFromNet){
                            CacheManager.getInstance().saveChapterFileWithHiragana(StringUtil.fromUrl2BookId(chapterEntity.getUrl()), chapterEntity.getChapter(), kanji, hiragana);
                        }
                        if (mView != null) mView.showChapterRead(chapterEntity.getChapter());
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                        LogUtils.e(throwable.toString());
                        if (mView != null) mView.netError(chapterEntity.getChapter());
                    }
                });
    }

}
