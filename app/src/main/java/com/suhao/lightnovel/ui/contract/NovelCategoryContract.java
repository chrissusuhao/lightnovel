package com.suhao.lightnovel.ui.contract;

import com.suhao.lightnovel.base.BaseContract;
import com.suhao.lightnovel.entity.CategoryEntity;
import com.suhao.lightnovel.entity.ChapterEntity;

import java.util.ArrayList;
import java.util.List;

public interface NovelCategoryContract {

    interface View extends BaseContract.BaseView {
        void showNovels(List<CategoryEntity> data);
        void gotoReadActivity(List<ChapterEntity> chapterList);

        void netError(Exception e);//添加网络处理异常接口
    }

    interface Presenter<T> extends BaseContract.BasePresenter<T> {
        void getNovels(int type, int page);
        void getShortStory(CategoryEntity categoryEntity);
        void getCatalogue(CategoryEntity categoryEntity);
    }

}
