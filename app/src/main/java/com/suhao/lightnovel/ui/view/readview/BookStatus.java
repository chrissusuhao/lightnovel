package com.suhao.lightnovel.ui.view.readview;

public enum BookStatus {

    NO_PRE_PAGE,
    NO_NEXT_PAGE,

    PRE_CHAPTER_LOAD_FAILURE,
    NEXT_CHAPTER_LOAD_FAILURE,

    LOAD_SUCCESS


}
