package com.suhao.lightnovel.module.novel.category;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;

import com.suhao.lightnovel.MyApplication;
import com.suhao.lightnovel.R;

public class NovelCategoryViewPagerAdapter extends FragmentPagerAdapter {

    final int PAGE_COUNT = 75;
    private String[] titles = MyApplication.getInstance().getString(R.string.tab)
                .replace("\r\n", "")
                .replace("\n", "")
                .split(",");

    public NovelCategoryViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return NovelCategoryFragment.newInstance(position);
    }

    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public int getItemPosition(Object object) {
        return PagerAdapter.POSITION_NONE;
    }

}