package com.suhao.lightnovel.module.novel.category;

import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.text.SpannableStringBuilder;
import android.view.View;
import android.view.View.OnClickListener;

import com.blankj.utilcode.util.SnackbarUtils;
import com.blankj.utilcode.util.SpanUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.suhao.lightnovel.MyApplication;
import com.suhao.lightnovel.R;
import com.suhao.lightnovel.activity.HomeActivity;
import com.suhao.lightnovel.entity.CategoryEntity;
import com.suhao.lightnovel.manager.communication.FunctionManager;
import com.suhao.lightnovel.module.CategoryAdapter;

import java.util.List;

public abstract class NovelCategoryAdapter extends CategoryAdapter {

    public NovelCategoryAdapter(@Nullable List<CategoryEntity> data) {
        super(data);
    }

    @Override
    protected void convert(final CategoryViewHolder holder, final CategoryEntity item) {
        super.convert(holder, item);
        if (item != null) {
            //TODO 判断是否有被添加到书架，如果已添加则不显示

            holder.getView(R.id.btnJoinCollection).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    MyApplication.getInstance().getLiteOrm().save(item);
                    FunctionManager.getInstance().invokeFunction(HomeActivity.FUNC_BOOKRACK_SNACK_BAR, true);
                }
            });
        }
    }
}
