package com.suhao.lightnovel.module;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.suhao.lightnovel.R;
import com.suhao.lightnovel.base.BaseFragment;
import com.suhao.lightnovel.entity.CategoryEntity;
import com.suhao.lightnovel.ui.view.dialog.BookLoadingDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public abstract class CategoryBaseFragment extends BaseFragment implements OnRefreshListener {
    protected static final String ARG_POSITION = "position";
    protected int position;

    @BindView(R.id.operators_recycler)
    RecyclerView mRecyclerView;
    @BindView(R.id.operators_refresh)
    SwipeRefreshLayout mRefreshLayout;

    protected List<CategoryEntity> data = new ArrayList<>();
    protected CategoryAdapter adapter;
    protected int page = 1;
    public String title;

    protected static final BookLoadingDialog bookLoadingDialog = new BookLoadingDialog();

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.fragment_category;
    }

    protected abstract void fillData();

    protected abstract void loadMoreRequested();

    @Override
    protected void init(Bundle savedInstanceState) {
        fillData();
        adapter.openLoadAnimation(BaseQuickAdapter.SCALEIN);
        adapter.setEnableLoadMore(true);
        adapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {

            @Override
            public void onLoadMoreRequested() {
                loadMoreRequested();
            }
        }, mRecyclerView);

        mRefreshLayout.setColorSchemeColors(Color.RED, Color.BLUE, Color.GREEN);
        mRefreshLayout.setOnRefreshListener(this);

//        mRecyclerView.addOnScrollListener(new ShowHideToolbarOnScrollingListener((Toolbar) getActivity().findViewById(R.id.common_toolbar)));

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));

        mRecyclerView.setAdapter(adapter);
    }

    @Override
    public void onRefresh() {
        mRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mRefreshLayout.setRefreshing(false);
            }
        });
    }
}