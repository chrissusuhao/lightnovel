package com.suhao.lightnovel.module.novel.bookrack;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;

import com.suhao.lightnovel.MyApplication;
import com.suhao.lightnovel.R;
import com.suhao.lightnovel.module.novel.category.NovelCategoryFragment;

public class NovelBookrackViewPagerAdapter extends FragmentPagerAdapter {

    final int PAGE_COUNT = 1;
    private String[] titles = new String[]{MyApplication.getInstance().getString(R.string.bookrack)};

    public NovelBookrackViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return NovelBookrackFragment.newInstance(position);
    }

    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public int getItemPosition(Object object) {
        return PagerAdapter.POSITION_NONE;
    }
}