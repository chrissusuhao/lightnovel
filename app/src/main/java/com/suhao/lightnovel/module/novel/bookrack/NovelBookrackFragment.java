package com.suhao.lightnovel.module.novel.bookrack;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.suhao.lightnovel.R;
import com.suhao.lightnovel.activity.HomeActivity;
import com.suhao.lightnovel.manager.communication.FunctionManager;
import com.suhao.lightnovel.module.novel.category.NovelCategoryAdapter;
import com.suhao.lightnovel.module.novel.read.ReadActivity;
import com.suhao.lightnovel.component.DaggerNovelComponent;
import com.suhao.lightnovel.entity.CategoryEntity;
import com.suhao.lightnovel.entity.ChapterEntity;
import com.suhao.lightnovel.module.CategoryBaseFragment;
import com.suhao.lightnovel.ui.contract.NovelBookrackContract;
import com.suhao.lightnovel.ui.contract.NovelCategoryContract;
import com.suhao.lightnovel.ui.presenter.NovelBookrackPresenter;
import com.suhao.lightnovel.ui.view.dialog.BookLoadingDialog;
import com.suhao.lightnovel.ui.presenter.NovelCategoryPresenter;

import java.util.List;

import javax.inject.Inject;

/**
 * 书架 Fragment
 */

public class NovelBookrackFragment extends CategoryBaseFragment implements NovelBookrackContract.View {

    @Inject
    NovelBookrackPresenter mPresenter;

    public static NovelBookrackFragment newInstance(int position) {
        NovelBookrackFragment f = new NovelBookrackFragment();
        Bundle b = new Bundle();
        b.putInt(ARG_POSITION, position);
        f.setArguments(b);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        DaggerNovelComponent.builder().build().inject(this);
        mPresenter.attachView(this);
        position = getArguments().getInt(ARG_POSITION);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    protected void fillData() {
        adapter = new NovelBookrackAdapter(data) {
            @Override
            public void onItemClick(int position) {
                itemClick(position);
            }
        };
        mPresenter.getNovels(position, page);
    }

    @Override
    protected void loadMoreRequested() {
        LogUtils.d("loadMoreRequested");
        adapter.setEnableLoadMore(false);
    }

    protected void itemClick(int position) {
        bookLoadingDialog.show(getChildFragmentManager(), "bookLoadingDialog");
        if (data.get(position).getNovelStatus().contains("短編")) {
            mPresenter.getShortStory(data.get(position));
        } else {
            mPresenter.getCatalogue(data.get(position));
        }
    }

    @Override
    public void showNovels(List<CategoryEntity> data) {
        FunctionManager.getInstance().invokeFunction(HomeActivity.FUNC_BOOKRACK_EMPTY, data.size() == 0);
        this.data.addAll(data);
        if (adapter.isLoading()) adapter.loadMoreComplete();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void gotoReadActivity(List<ChapterEntity> chapterList) {
        bookLoadingDialog.dismissAllowingStateLoss();
        ReadActivity.startActivity(getActivity(), chapterList);
    }

    @Override
    public void netError(Exception e) {
        ToastUtils.showLong(R.string.net_error);
        LogUtils.e(e);
    }

    @Override
    public void showError() {
        hideDialog();
    }

    @Override
    public void complete() {
        hideDialog();
    }

    public void hideDialog() {
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mPresenter != null) mPresenter.detachView();
    }
}