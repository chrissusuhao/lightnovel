package com.suhao.lightnovel.module;

import android.support.annotation.Nullable;
import android.view.View;
import android.view.View.OnClickListener;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.suhao.lightnovel.R;
import com.suhao.lightnovel.entity.CategoryEntity;
import com.suhao.lightnovel.module.novel.category.CategoryViewHolder;

import java.util.List;

public abstract class CategoryAdapter extends BaseQuickAdapter<CategoryEntity, CategoryViewHolder> {

    public CategoryAdapter(@Nullable List<CategoryEntity> data) {
        super(R.layout.item_category, data);
    }

    @Override
    protected void convert(final CategoryViewHolder holder, CategoryEntity item) {
        if (item != null) {
            holder.setExpandableText(R.id.item_des, item.getDescription().trim())
                    .setText(R.id.item_title, item.getTitle())
                    .setText(R.id.item_author, item.getAuthor())
                    .setText(R.id.item_evaluate_point, item.getEvaluatePoint())
                    .setText(R.id.item_status, item.getNovelStatus());

            holder.itemView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClick(holder.getAdapterPosition());
                }
            });
        }
    }

    public abstract void onItemClick(int position);
}
