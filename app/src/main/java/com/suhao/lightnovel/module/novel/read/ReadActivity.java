package com.suhao.lightnovel.module.novel.read;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.ListPopupWindow;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.suhao.lightnovel.MyApplication;
import com.suhao.lightnovel.R;
import com.suhao.lightnovel.base.BaseActivity;
import com.suhao.lightnovel.base.Constant;
import com.suhao.lightnovel.component.DaggerNovelComponent;
import com.suhao.lightnovel.entity.BookMark;
import com.suhao.lightnovel.entity.ChapterEntity;
import com.suhao.lightnovel.manager.CacheManager;
import com.suhao.lightnovel.manager.SettingManager;
import com.suhao.lightnovel.manager.ThemeManager;
import com.suhao.lightnovel.manager.communication.FunctionManager;
import com.suhao.lightnovel.manager.communication.FunctionWithParamNoResult;
import com.suhao.lightnovel.service.DownloadBookService;
import com.suhao.lightnovel.service.bean.DownloadMessage;
import com.suhao.lightnovel.service.bean.DownloadProgress;
import com.suhao.lightnovel.service.bean.DownloadQueue;
import com.suhao.lightnovel.ui.contract.BookReadContract;
import com.suhao.lightnovel.ui.presenter.ReadPresenter;
import com.suhao.lightnovel.ui.view.readview.BaseReadView;
import com.suhao.lightnovel.ui.view.readview.NoAimWidget;
import com.suhao.lightnovel.ui.view.readview.OnReadStateChangeListener;
import com.suhao.lightnovel.ui.view.readview.OverlappedWidget;
import com.suhao.lightnovel.ui.view.readview.PageWidget;
import com.suhao.lightnovel.utils.ScreenUtil;
import com.suhao.lightnovel.utils.SharedPreferencesUtil;
import com.suhao.lightnovel.utils.StatusBarCompat;
import com.tencent.bugly.crashreport.CrashReport;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import thinkfreely.changemodelibrary.ChangeModeController;

/**
 * Created by bz on 2018/4/19.
 */

public class ReadActivity extends BaseActivity implements BookReadContract.View {

    public static final String FUNC_DOWNLOAD_PROGRESS = "download_progress";
    public static final String FUNC_DOWNLOAD_MESSAGE = "download_message";

    @BindView(R.id.ivBack)
    ImageView mIvBack;
    @BindView(R.id.tvBookReadReading)
    TextView mTvBookReadReading;

    @BindView(R.id.flReadWidget)
    FrameLayout flReadWidget;

    @BindView(R.id.llBookReadTop)
    LinearLayout mLlBookReadTop;
    @BindView(R.id.tvBookReadTocTitle)
    TextView mTvBookReadTocTitle;
    @BindView(R.id.tvBookReadMode)
    TextView mTvBookReadMode;
    @BindView(R.id.tvBookReadSettings)
    TextView mTvBookReadSettings;
    @BindView(R.id.tvBookReadDownload)
    TextView mTvBookReadDownload;
    @BindView(R.id.tvBookReadToc)
    TextView mTvBookReadToc;
    @BindView(R.id.llBookReadBottom)
    LinearLayout mLlBookReadBottom;
    @BindView(R.id.rlBookReadRoot)
    RelativeLayout mRlBookReadRoot;
    @BindView(R.id.tvDownloadProgress)
    TextView mTvDownloadProgress;

    @BindView(R.id.rlReadAaSet)
    LinearLayout rlReadAaSet;
    @BindView(R.id.tvFontsizeMinus)
    TextView tvFontsizeMinus;
    @BindView(R.id.seekbarFontSize)
    SeekBar seekbarFontSize;
    @BindView(R.id.tvFontsizePlus)
    TextView tvFontsizePlus;

    @BindView(R.id.tvHiraganaFontsizeMinus)
    TextView tvHiraganaFontsizeMinus;
    @BindView(R.id.tvHiraganaFontsizePlus)
    TextView tvHiraganaFontsizePlus;

    @BindView(R.id.rlReadMark)
    LinearLayout rlReadMark;
    @BindView(R.id.tvAddMark)
    TextView tvAddMark;
    @BindView(R.id.lvMark)
    ListView lvMark;

    @BindView(R.id.cbVolume)
    CheckBox cbVolume;
    @BindView(R.id.gvTheme)
    GridView gvTheme;

    private View decodeView;

    @Inject
    ReadPresenter mPresenter;

    private BaseReadView mPageWidget;
    private ListPopupWindow mTocListPopupWindow;
    private TocListAdapter mTocListAdapter;

    private List<BookMark> mMarkList;
    private BookMarkAdapter mMarkAdapter;
    private Receiver receiver = new Receiver();
    private IntentFilter intentFilter = new IntentFilter();
    private SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

    private List<ChapterEntity> mChapterList = new ArrayList<>();
    private String bookId;

    public static final String INTENT_CHAPTER_LIST = "chapterList";

    public static void startActivity(Context context, List<ChapterEntity> chapterList) {
        context.startActivity(new Intent(context, ReadActivity.class)
                .putParcelableArrayListExtra(INTENT_CHAPTER_LIST, (ArrayList<ChapterEntity>) chapterList));
    }

    @Override
    protected int getContentViewLayoutID() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        return R.layout.activity_read;
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        DaggerNovelComponent.builder().build().inject(this);
        mPresenter.attachView(this);
        decodeView = getWindow().getDecorView();
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mLlBookReadTop.getLayoutParams();
        params.topMargin = ScreenUtil.getStatusBarHeight(this) - 2;
        mLlBookReadTop.setLayoutParams(params);
        initTheme();
        showDialog();
        initData();
        initAASet();
        initPagerWidget();
        initTocList();
        initCommunication();
    }

    private void initCommunication() {
        FunctionManager.getInstance().addFunction(downloadProgress);
        FunctionManager.getInstance().addFunction(downloadMessage);
    }

    private FunctionWithParamNoResult<DownloadProgress> downloadProgress = new FunctionWithParamNoResult<DownloadProgress>(FUNC_DOWNLOAD_PROGRESS) {

        @Override
        public void function(final DownloadProgress downloadProgress) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (bookId.equals(downloadProgress.bookId)) {
                        if (isVisible(mLlBookReadBottom)) { // 如果工具栏显示，则进度条也显示
                            visible(mTvDownloadProgress);
                            // 如果之前缓存过，就给提示
                            mTvDownloadProgress.setText(downloadProgress.message);
                        } else {
                            gone(mTvDownloadProgress);
                        }
                    }
                }
            });
        }
    };

    private FunctionWithParamNoResult<DownloadMessage> downloadMessage = new FunctionWithParamNoResult<DownloadMessage>(FUNC_DOWNLOAD_MESSAGE) {

        @Override
        public void function(final DownloadMessage downloadMessage) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (isVisible(mLlBookReadBottom)) { // 如果工具栏显示，则进度条也显示
                        if (bookId.equals(downloadMessage.bookId)) {
                            visible(mTvDownloadProgress);
                            mTvDownloadProgress.setText(downloadMessage.message);
                            if (downloadMessage.isComplete) {
                                mTvDownloadProgress.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        gone(mTvDownloadProgress);
                                    }
                                }, 2500);
                            }
                        }
                    }
                }
            });
        }
    };

    private void initTheme() {
        int color = SettingManager.getInstance().getThemeColor();
        statusBarView = StatusBarCompat.compat(this, color);
        mLlBookReadTop.setBackgroundColor(color);
        mLlBookReadBottom.setBackgroundColor(color);
        rlReadAaSet.setBackgroundColor(color);
        rlReadMark.setBackgroundColor(color);
        changeModeTextAndIcon(SharedPreferencesUtil.getInstance().getBoolean(Constant.ISNIGHT));
        if (SharedPreferencesUtil.getInstance().getBoolean(Constant.ISNIGHT)) {
            ChangeModeController.changeNight(ReadActivity.this, R.style.NightTheme);
        }
    }

    private void initData() {
        mChapterList = getIntent().getParcelableArrayListExtra(INTENT_CHAPTER_LIST);
        mPresenter.getChapterRead(mChapterList.get(0));
        bookId = mChapterList.get(0).getBookId();
    }

    private void initAASet() {
        curTheme = SettingManager.getInstance().getReadTheme();
        ThemeManager.setReaderTheme(curTheme, mRlBookReadRoot);

        seekbarFontSize.setMax(10);
        //int fontSizePx = SettingManager.getInstance().getReadFontSize(bookId);
        int fontSizePx = SettingManager.getInstance().getReadFontSize();
        int progress = (int) ((ScreenUtil.pxToDpInt(fontSizePx) - 12) / 1.7f);
        seekbarFontSize.setProgress(progress);
        seekbarFontSize.setOnSeekBarChangeListener(new SeekBarChangeListener());

        cbVolume.setChecked(SettingManager.getInstance().isVolumeFlipEnable());
        cbVolume.setOnCheckedChangeListener(new ChechBoxChangeListener());

        gvAdapter = new ReadThemeAdapter(this, (themes = ThemeManager.getReaderThemeData(curTheme)), curTheme);
        gvTheme.setAdapter(gvAdapter);
        gvTheme.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position < themes.size() - 1) {
                    changedMode(false, position);
                } else {
                    changedMode(true, position);
                }
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                if (mTocListPopupWindow != null && mTocListPopupWindow.isShowing()) {
                    mTocListPopupWindow.dismiss();
                    gone(mTvBookReadTocTitle);
                    visible(mTvBookReadReading);
                    return true;
                } else if (isVisible(rlReadAaSet)) {
                    gone(rlReadAaSet);
                    return true;
                } else if (isVisible(mLlBookReadBottom)) {
                    hideReadBar();
                    return true;
                }
                break;
            case KeyEvent.KEYCODE_MENU:
                toggleReadBar();
                return true;
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                if (SettingManager.getInstance().isVolumeFlipEnable()) {
                    return true;
                }
                break;
            case KeyEvent.KEYCODE_VOLUME_UP:
                if (SettingManager.getInstance().isVolumeFlipEnable()) {
                    return true;
                }
                break;
            default:
                break;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            if (SettingManager.getInstance().isVolumeFlipEnable()) {
                mPageWidget.nextPage();
                return true;// 防止翻页有声音
            }
        } else if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
            if (SettingManager.getInstance().isVolumeFlipEnable()) {
                mPageWidget.prePage();
                return true;
            }
        }
        return super.onKeyUp(keyCode, event);
    }

    class Receiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (mPageWidget != null) {
                if (Intent.ACTION_BATTERY_CHANGED.equals(intent.getAction())) {
                    int level = intent.getIntExtra("level", 0);
                    mPageWidget.setBattery(100 - level);
                } else if (Intent.ACTION_TIME_TICK.equals(intent.getAction())) {
                    mPageWidget.setTime(sdf.format(new Date()));
                }
            }
        }
    }

    private void initPagerWidget() {
        switch (SharedPreferencesUtil.getInstance().getInt(Constant.FLIP_STYLE, 0)) {
            case 0:
                mPageWidget = new PageWidget(this, mChapterList.get(0).getBookId(), mChapterList, new ReadListener());
                break;
            case 1:
                mPageWidget = new OverlappedWidget(this, mChapterList.get(0).getBookId(), mChapterList, new ReadListener());
                break;
            case 2:
                mPageWidget = new NoAimWidget(this, mChapterList.get(0).getBookId(), mChapterList, new ReadListener());
        }
        intentFilter.addAction(Intent.ACTION_BATTERY_CHANGED);
        intentFilter.addAction(Intent.ACTION_TIME_TICK);
        registerReceiver(receiver, intentFilter);
        if (SharedPreferencesUtil.getInstance().getBoolean(Constant.ISNIGHT, false)) {
            mPageWidget.setTextColor(ContextCompat.getColor(this, R.color.chapter_content_night),
                    ContextCompat.getColor(this, R.color.chapter_title_night));
        }
        flReadWidget.removeAllViews();
        flReadWidget.addView(mPageWidget);
    }

    private class ChechBoxChangeListener implements CompoundButton.OnCheckedChangeListener {

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (buttonView.getId() == cbVolume.getId()) {
                SettingManager.getInstance().saveVolumeFlipEnable(isChecked);
            }
        }
    }

    private int currentChapter = 1;

    private class ReadListener implements OnReadStateChangeListener {
        @Override
        public void onChapterChanged(int chapter) {
            LogUtils.i("onChapterChanged:" + chapter);
            currentChapter = chapter;
            mTocListAdapter.setCurrentChapter(currentChapter);
            // 加载前一节 与 后三节
            for (int i = chapter - 1; i <= chapter + 3 && i <= mChapterList.size(); i++) {
                if (i > 0 && i != chapter
                        && !CacheManager.getInstance().isChapterFileCached(bookId, i)) {
                    mPresenter.getChapterRead(mChapterList.get(i - 1));
                }
            }
        }

        @Override
        public void onPageChanged(int chapter, int page) {
            LogUtils.i("onPageChanged:" + chapter + "-" + page);
            if (mPageWidget.isTTSWorking()) {
                LogUtils.i("isTTSWorking:" + chapter + "-" + page);
                mPageWidget.ttsSpeak();
            }
        }

        @Override
        public void onLoadChapterFailure(int chapter) {
            LogUtils.i("onLoadChapterFailure:" + chapter);
            startRead = false;
            if (CacheManager.getInstance().getChapterFile(bookId, chapter) == null)
                mPresenter.getChapterRead(mChapterList.get(chapter - 1));
        }

        @Override
        public void onCenterClick() {
            LogUtils.i("onCenterClick");
            toggleReadBar();
        }

        @Override
        public void onFlip() {
            hideReadBar();
        }
    }

    private class SeekBarChangeListener implements SeekBar.OnSeekBarChangeListener {

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (seekBar.getId() == seekbarFontSize.getId() && fromUser) {
                calcFontSize(progress);
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    }

    private void initTocList() {
        mTocListAdapter = new TocListAdapter(this, mChapterList, mChapterList.get(0).getBookId(), currentChapter);
        mTocListPopupWindow = new ListPopupWindow(this);
        mTocListPopupWindow.setAdapter(mTocListAdapter);
        mTocListPopupWindow.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        mTocListPopupWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        mTocListPopupWindow.setAnchorView(mLlBookReadTop);
        mTocListPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mTocListPopupWindow.dismiss();
                currentChapter = position + 1;
                mTocListAdapter.setCurrentChapter(currentChapter);
                startRead = false;
                showDialog();
                readCurrentChapter();
                hideReadBar();
            }
        });
        mTocListPopupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                gone(mTvBookReadTocTitle);
                visible(mTvBookReadReading);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPageWidget.ttsStop();
    }

    /**
     * 获取当前章节。章节文件存在则直接阅读，不存在则请求
     */
    public void readCurrentChapter() {
        if (CacheManager.getInstance().isChapterFileCached(mChapterList.get(0).getBookId(), currentChapter)) {
            showChapterRead(currentChapter);
        } else {
            showDialog();
            mPresenter.getChapterRead(mChapterList.get(currentChapter - 1));
        }
    }
    private int seekbarFontSizeProgress;
    private void calcFontSize(int progress) {
        // progress range 1 - 10
        if (progress >= 0 && progress <= 10) {
            seekbarFontSize.setProgress(progress);
            seekbarFontSizeProgress = progress;
            mPageWidget.setFontSize(ScreenUtil.dpToPxInt(12 + 1.7f * progress));
        }
    }

    private void calcHiraganaFontSize(int progress) {
        if (progress <= seekbarFontSize.getProgress())
            mPageWidget.setHiraganaFontSize(ScreenUtil.dpToPxInt(12 + 1.7f * progress));
    }

    @Override
    public void showError() {

    }

    @Override
    public void complete() {

    }

    private boolean startRead = false;
    private int curTheme = -1;

    @Override
    public void showChapterRead(int chapter) {
        if (!startRead) {
            startRead = true;
            currentChapter = chapter;
            if (!mPageWidget.isPrepared) {
                mPageWidget.init(curTheme);
            } else {
                mPageWidget.jumpToChapter(currentChapter);
            }
            hideDialog();
        }
    }

    @Override
    public void netError(int chapter) {
        hideDialog();//防止因为网络问题而出现dialog不消失
        if (Math.abs(chapter - currentChapter) <= 1) {
            ToastUtils.showShort(R.string.net_error);
        }
    }

    private synchronized void toggleReadBar() { // 切换工具栏 隐藏/显示 状态
        if (isVisible(mLlBookReadTop)) {
            hideReadBar();
        } else {
            showReadBar();
        }
    }

    private synchronized void showReadBar() { // 显示工具栏
        visible(mLlBookReadBottom, mLlBookReadTop);
        showStatusBar();
        decodeView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    private synchronized void hideReadBar() {
        gone(mTvDownloadProgress, mLlBookReadBottom, mLlBookReadTop, rlReadAaSet, rlReadMark);
        hideStatusBar();
        decodeView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);
    }

    @OnClick(R.id.ivBack)
    public void onClickBack() {
        if (mTocListPopupWindow.isShowing()) {
            mTocListPopupWindow.dismiss();
        } else {
            finish();
        }
    }

    @OnClick(R.id.tvBookReadReading)
    public void readBook() {
        gone(rlReadAaSet, rlReadMark);
        if (mPageWidget.isTTSWorking()) {
            mPageWidget.ttsStop();
            mTvBookReadReading.setText(R.string.book_read_reading);
        } else {
            mPageWidget.ttsPlay();
            mTvBookReadReading.setText(R.string.book_read_reading_pause);
        }
    }

    /***************Setting Menu*****************/

    @OnClick(R.id.tvFontsizeMinus)
    public void fontsizeMinus() {
        calcFontSize(seekbarFontSize.getProgress() - 1);
    }

    @OnClick(R.id.tvFontsizePlus)
    public void fontsizePlus() {
        calcFontSize(seekbarFontSize.getProgress() + 1);
    }

    @OnClick(R.id.tvHiraganaFontsizeMinus)
    public void hiraganaFontsizeMinus() {
        calcHiraganaFontSize(--seekbarFontSizeProgress);
    }

    @OnClick(R.id.tvHiraganaFontsizePlus)
    public void hiraganaFontsizePlus() {
        calcHiraganaFontSize(++seekbarFontSizeProgress);
    }


    /***************Book Mark*****************/

    @OnClick(R.id.tvAddMark)
    public void addBookMark() {
        int[] readPos = mPageWidget.getReadPos();
        BookMark mark = new BookMark();
        mark.chapter = readPos[0];
        mark.page = readPos[1];
        if (mark.chapter >= 1 && mark.chapter <= mChapterList.size()) {
            mark.title = mChapterList.get(currentChapter - 1).getTitle();
        }
        mark.desc = mPageWidget.getHeadLine();
        if (SettingManager.getInstance().addBookMark(bookId, mark)) {
            ToastUtils.showShort("添加书签成功");
            updateMark();
        } else {
            ToastUtils.showShort("书签已存在");
        }
    }

    private void updateMark() {
        if (mMarkAdapter == null) {
            mMarkAdapter = new BookMarkAdapter(this, new ArrayList<BookMark>());
            lvMark.setAdapter(mMarkAdapter);
            lvMark.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    BookMark mark = mMarkAdapter.getData(position);
                    if (mark != null) {
                        mPageWidget.setPosition(new int[]{mark.chapter, mark.page});
                        hideReadBar();
                    } else {
                        ToastUtils.showShort("书签无效");
                    }
                }
            });
        }
        mMarkAdapter.clear();

        mMarkList = SettingManager.getInstance().getBookMarks(bookId);
        if (mMarkList != null && mMarkList.size() > 0) {
            Collections.reverse(mMarkList);
            mMarkAdapter.addAll(mMarkList);
        }
    }

    @OnClick(R.id.tvClear)
    public void clearBookMark() {
        SettingManager.getInstance().clearBookMarks(bookId);

        updateMark();
    }

    /***************Bottom Bar*****************/

    private List<ReadTheme> themes;
    private ReadThemeAdapter gvAdapter;

    @OnClick(R.id.tvBookReadMode)
    public void onClickChangeMode() { // 日/夜间模式切换
        gone(rlReadAaSet, rlReadMark);

        boolean isNight = !SharedPreferencesUtil.getInstance().getBoolean(Constant.ISNIGHT, false);
        changedMode(isNight, -1);
    }

    private void changedMode(boolean isNight, int position) {
        SharedPreferencesUtil.getInstance().putBoolean(Constant.ISNIGHT, isNight);
        if (isNight) {
            ChangeModeController.changeNight(ReadActivity.this, R.style.NightTheme);
        } else {
            initTheme();
        }
        AppCompatDelegate.setDefaultNightMode(isNight ? AppCompatDelegate.MODE_NIGHT_YES
                : AppCompatDelegate.MODE_NIGHT_NO);

        if (position >= 0) {
            curTheme = position;
        } else {
            curTheme = SettingManager.getInstance().getReadTheme();
        }
        gvAdapter.select(curTheme);

        mPageWidget.setTheme(isNight ? ThemeManager.NIGHT : curTheme);
        mPageWidget.setTextColor(ContextCompat.getColor(mContext, isNight ? R.color.chapter_content_night : R.color.chapter_content_day),
                ContextCompat.getColor(mContext, isNight ? R.color.chapter_title_night : R.color.chapter_title_day));
        changeModeTextAndIcon(isNight);
        ThemeManager.setReaderTheme(curTheme, mRlBookReadRoot);
    }

    private void changeModeTextAndIcon(boolean isNight){
        mTvBookReadMode.setText(getString(isNight ? R.string.book_read_mode_day_manual_setting
                : R.string.book_read_mode_night_manual_setting));
        Drawable drawable = ContextCompat.getDrawable(this, isNight ? R.drawable.ic_menu_mode_day_manual
                : R.drawable.ic_menu_mode_night_manual);
        drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
        mTvBookReadMode.setCompoundDrawables(null, drawable, null, null);
    }

    @OnClick(R.id.tvBookReadSettings)
    public void setting() {
        if (isVisible(mLlBookReadBottom)) {
            if (isVisible(rlReadAaSet)) {
                gone(rlReadAaSet);
            } else {
                visible(rlReadAaSet);
                gone(rlReadMark);
            }
        }
    }

    @OnClick(R.id.tvBookReadDownload)
    public void downloadBook() {
        gone(rlReadAaSet);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.cache_dialog_title))
                .setItems(new String[]{getString(R.string.cache_50_chapter), getString(R.string.cache_all_after),
                        getString(R.string.cache_all)}, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                DownloadBookService.post(new DownloadQueue(bookId, mChapterList, currentChapter + 1, currentChapter + 50));
                                break;
                            case 1:
                                DownloadBookService.post(new DownloadQueue(bookId, mChapterList, currentChapter + 1, mChapterList.size()));
                                break;
                            case 2:
                                DownloadBookService.post(new DownloadQueue(bookId, mChapterList, 1, mChapterList.size()));
                                break;
                            default:
                                break;
                        }
                    }
                });
        builder.show();
    }

    @OnClick(R.id.tvBookMark)
    public void onClickMark() {
        if (isVisible(mLlBookReadBottom)) {
            if (isVisible(rlReadMark)) {
                gone(rlReadMark);
            } else {
                gone(rlReadAaSet);
                updateMark();
                visible(rlReadMark);
            }
        }
    }

    @OnClick(R.id.tvBookReadToc)
    public void onClickToc() {
        gone(rlReadAaSet, rlReadMark);
        if (!mTocListPopupWindow.isShowing()) {
            visible(mTvBookReadTocTitle);
            gone(mTvBookReadReading);
            mTocListPopupWindow.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
            mTocListPopupWindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
            mTocListPopupWindow.show();
            mTocListPopupWindow.setSelection(currentChapter - 1);
            mTocListPopupWindow.getListView().setFastScrollEnabled(true);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPageWidget.ttsStop();
        if (mPresenter != null) {
            mPresenter.detachView();
        }

        try {
            unregisterReceiver(receiver);
        } catch (Exception e) {
            LogUtils.e("Receiver not registered");
        }

        // 观察内存泄漏情况
        MyApplication.getRefWatcher(this).watch(this);
    }
}
