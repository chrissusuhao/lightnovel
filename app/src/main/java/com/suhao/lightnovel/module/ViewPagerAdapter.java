package com.suhao.lightnovel.module;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.ViewGroup;

import com.blankj.utilcode.util.LogUtils;
import com.suhao.lightnovel.ui.view.SlidingTabLayout;

import java.util.ArrayList;

/**
 * Created by DAT on 8/16/2015.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private final ArrayList<Fragment> mFragmentList = new ArrayList<>();
    private final ArrayList<String> mFragmentTitleList = new ArrayList<>();
    Context mContext;
    ViewPager mViewPager;
    SlidingTabLayout mSlidingTabLayout;
    FragmentManager mFragmentManager;

    public ViewPagerAdapter(FragmentManager manager, Context context, ViewPager viewPager,
                            SlidingTabLayout slidingTabLayout) {
        super(manager);
        mFragmentManager = manager;
        mContext = context;
        mViewPager = viewPager;
        mSlidingTabLayout = slidingTabLayout;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFrag(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    public void removeFrag(int position) {
        Fragment fragment = mFragmentList.get(position);
        mFragmentList.remove(fragment);
        mFragmentTitleList.remove(position);
        destroyFragmentView(mViewPager, position, fragment);
    }

    public void removeAllFrag() {
        int size = mFragmentList.size();
        for (int i = size - 1; i >= 0; i--) {
            removeFrag(i);
        }
        if (getCount() > 0) mSlidingTabLayout.setViewPager(mViewPager);
    }

    public void destroyFragmentView(ViewGroup container, int position, Object object) {
        FragmentTransaction trans = mFragmentManager.beginTransaction();
        trans.remove((Fragment) object);
        trans.commit();
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }
}
