package com.suhao.lightnovel.module.novel.category;

import android.support.annotation.IdRes;
import android.view.View;

import com.chad.library.adapter.base.BaseViewHolder;
import com.ms.square.android.expandabletextview.ExpandableTextView;

/**
 * Created by Administrator on 4/21/2018.
 */

public class CategoryViewHolder extends BaseViewHolder {

    public CategoryViewHolder(View view) {
        super(view);
    }

    public CategoryViewHolder setExpandableText(@IdRes int viewId, CharSequence value) {
        ExpandableTextView view = getView(viewId);
        view.setText(value);
        return this;
    }
}
