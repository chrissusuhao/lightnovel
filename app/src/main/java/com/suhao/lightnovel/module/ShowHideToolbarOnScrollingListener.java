package com.suhao.lightnovel.module;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

public class ShowHideToolbarOnScrollingListener extends RecyclerView.OnScrollListener {

    private Toolbar mToolbar;
    private int mToolbarHeight = 0;
    private float mToolbarTranslationY = 0;

    public ShowHideToolbarOnScrollingListener(Toolbar toolbar) {
        mToolbar = toolbar;
        mToolbarHeight = mToolbar.getHeight();
        mToolbarTranslationY = mToolbar.getTranslationY();
    }

    @Override
    public final void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        if (newState == RecyclerView.SCROLL_STATE_IDLE) {
            switchToolbarStatus(recyclerView);
        }
    }

    @Override
    public final void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        onMoveToolbar(dy);
    }

    private void switchToolbarStatus(RecyclerView recyclerView) {
        if (mToolbarTranslationY >= 0 || Math.abs(mToolbarTranslationY) >= mToolbarHeight) return;
        boolean showOrHide = mToolbarTranslationY >= -(mToolbarHeight / 2) || recyclerView.getScrollY() <= mToolbarHeight / 2;
        if (showOrHide) {
            mToolbarTranslationY = 0;
            mToolbar.animate().translationY(0).start();
        } else {
            mToolbarTranslationY = -mToolbarHeight;
            mToolbar.animate().translationY(-mToolbarHeight).start();
        }
    }

    private void onMoveToolbar(int dy) {
        mToolbarHeight = mToolbar.getHeight();
        if (dy == 0) return;
        if (mToolbarTranslationY == 0 && dy < 0) return;
        if (mToolbarTranslationY == -mToolbarHeight && dy > 0) return;
        mToolbarTranslationY = mToolbarTranslationY - dy;
        if (mToolbarTranslationY > 0) {
            mToolbarTranslationY = 0;
        } else if (mToolbarTranslationY < -mToolbarHeight) {
            mToolbarTranslationY = -mToolbarHeight;
        }
        mToolbar.setTranslationY(mToolbarTranslationY);
    }
}

