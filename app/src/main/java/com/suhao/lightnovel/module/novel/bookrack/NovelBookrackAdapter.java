package com.suhao.lightnovel.module.novel.bookrack;

import android.content.res.ColorStateList;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.suhao.lightnovel.MyApplication;
import com.suhao.lightnovel.R;
import com.suhao.lightnovel.activity.HomeActivity;
import com.suhao.lightnovel.entity.CategoryEntity;
import com.suhao.lightnovel.manager.communication.FunctionManager;
import com.suhao.lightnovel.module.CategoryAdapter;
import com.suhao.lightnovel.module.novel.category.CategoryViewHolder;
import com.suhao.lightnovel.ui.view.DrawableCenterButton;

import java.util.List;

public abstract class NovelBookrackAdapter extends CategoryAdapter {

    public NovelBookrackAdapter(@Nullable List<CategoryEntity> data) {
        super(data);
    }

    @Override
    protected void convert(final CategoryViewHolder holder, final CategoryEntity item) {
        super.convert(holder, item);
        if (item != null) {
            DrawableCenterButton drawableCenterButton = holder.getView(R.id.btnJoinCollection);
            drawableCenterButton.setText(R.string.remove_from_bookrack);
            drawableCenterButton.setCompoundDrawables(ContextCompat.getDrawable(mContext, R.drawable.book_detail_info_del_img), null, null, null);
            drawableCenterButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mData.remove(item);
                    MyApplication.getInstance().getLiteOrm().delete(item);
                    notifyDataSetChanged();
                    FunctionManager.getInstance().invokeFunction(HomeActivity.FUNC_BOOKRACK_SNACK_BAR, false);
                }
            });
        }
    }
}
