package com.suhao.lightnovel.manager;

import com.blankj.utilcode.util.LogUtils;
import com.suhao.lightnovel.MyApplication;
import com.suhao.lightnovel.base.Constant;
import com.suhao.lightnovel.utils.FileUtil;
import com.suhao.lightnovel.utils.SharedPreferencesUtil;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by bz on 2018/4/18.
 */

public class CacheManager {
    public static final String HIRAGANA_SUFFIX = "_hiragana";
    private static CacheManager manager;

    public static CacheManager getInstance() {
        return manager == null ? (manager = new CacheManager()) : manager;
    }

    public void saveChapterFileWithHiragana(String bookId, int chapter, ArrayList<String> kanji, ArrayList<String> hiragana) {
        File fileKanji = FileUtil.getChapterFile(bookId, chapter);
        FileUtil.writeFile(fileKanji.getAbsolutePath(), kanji.toString().substring(1, kanji.toString().length() - 1), false);
        File fileHiragana = FileUtil.getChapterFile(bookId, chapter + HIRAGANA_SUFFIX);
        FileUtil.writeFile(fileHiragana.getAbsolutePath(), hiragana.toString().substring(1, hiragana.toString().length() - 1), false);
    }

    /**
     * @return true 为已缓存 false 未缓存
     */
    public boolean isChapterFileCached(String bookId, int chapter) {
        File fileKanji = FileUtil.getChapterFile(bookId, chapter);
        return fileKanji != null && fileKanji.length() > 50;
    }

    public File getChapterFile(String bookId, int chapter) {
        File file = FileUtil.getChapterFile(bookId, chapter);
        if (file != null && file.length() > 50)
            return file;
        return null;
    }

    /**
     * 获取缓存大小
     *
     * @return
     */
    public synchronized String getCacheSize() {
        long cacheSize = 0;

        try {
            String cacheDir = Constant.BASE_PATH;
            cacheSize += FileUtil.getFolderSize(cacheDir);
            if (FileUtil.isSdCardAvailable()) {
                String extCacheDir = MyApplication.getInstance().getExternalCacheDir().getPath();
                cacheSize += FileUtil.getFolderSize(extCacheDir);
            }
        } catch (Exception e) {
            LogUtils.e(e.toString());
        }

        return FileUtil.formatFileSizeToString(cacheSize);
    }

    /**
     * 清除缓存
     *
     * @param clearReadPos 是否删除阅读记录
     */
    public synchronized void clearCache(boolean clearReadPos, boolean clearCollect) {
        try {
            // 删除内存缓存
            String cacheDir = MyApplication.getInstance().getCacheDir().getPath();
            FileUtil.deleteFileOrDirectory(new File(cacheDir));
            if (FileUtil.isSdCardAvailable()) {
                // 删除SD书籍缓存
                FileUtil.deleteFileOrDirectory(new File(Constant.PATH_DATA));
            }
            // 删除阅读记录（SharePreference）
            if (clearReadPos) {
                //防止再次弹出性别选择框，sp要重写入保存的性别
                SharedPreferencesUtil.getInstance().removeAll();
            }
            // 清空书架
            if (clearCollect) {
                MyApplication.getInstance().getLiteOrm().dropTable("category_entity");
            }
        } catch (Exception e) {
            LogUtils.e(e.toString());
        }
    }
}
