package com.suhao.lightnovel.manager;

import android.graphics.Color;

import com.blankj.utilcode.util.LogUtils;
import com.suhao.lightnovel.base.Constant;
import com.suhao.lightnovel.entity.BookMark;
import com.suhao.lightnovel.utils.ScreenUtil;
import com.suhao.lightnovel.utils.SharedPreferencesUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者：suhao on 2018/4/19.
 * 邮箱：249674239@qq.com
 */

public class SettingManager {

    private volatile static SettingManager manager;

    public static SettingManager getInstance() {
        return manager != null ? manager : (manager = new SettingManager());
    }

    public int getReadFontSize() {
        return SharedPreferencesUtil.getInstance().getInt(getFontSizeKey(""), ScreenUtil.dpToPxInt(16));
    }
    public int getHiraganaFontSize() {
        return SharedPreferencesUtil.getInstance().getInt(getFontSizeKey("hiragana"), ScreenUtil.dpToPxInt(12));
    }

    private String getFontSizeKey(String bookId) {
        return bookId + "-readFontSize";
    }

    /**
     * 获取上次阅读章节及位置
     *
     * @param bookId
     * @return
     */
    public int[] getReadProgress(String bookId) {
        int[] progress = new int[2];
        progress[0] = SharedPreferencesUtil.getInstance().getInt(getChapterKey(bookId), 1);
        progress[1] = SharedPreferencesUtil.getInstance().getInt(getIndexPage(bookId) + "-" + progress[0], 1);
        return progress;
    }

    public synchronized void saveReadProgress(String bookId, int currentChapter, int indexPage) {
        SharedPreferencesUtil.getInstance().putInt(getChapterKey(bookId), currentChapter);
        SharedPreferencesUtil.getInstance().putInt(getIndexPage(bookId) + "-" + currentChapter, indexPage);
    }

    public synchronized void saveReadProgressChapter(String bookId, int currentChapter, int indexPage) {
        SharedPreferencesUtil.getInstance().putInt(getIndexPage(bookId) + "-" + currentChapter, indexPage);
    }

    public int getReadProgressChapter(String bookId, int currentChapter) {
        return SharedPreferencesUtil.getInstance().getInt(getIndexPage(bookId) + "-" + currentChapter, 0);
    }

    private String getChapterKey(String bookId) {
        return bookId + "-chapter";
    }

    private String getIndexPage(String bookId) {
        return bookId + "-indexPage";
    }

    private String getLineDownDraw(String bookId) {
        return bookId + "-lineDownDraw";
    }

    private String getStartX(String bookId) {
        return bookId + "-startX";
    }
    /**
     * 是否可以使用音量键翻页
     *
     * @param enable
     */
    public void saveVolumeFlipEnable(boolean enable) {
        SharedPreferencesUtil.getInstance().putBoolean("volumeFlip", enable);
    }
    public boolean isVolumeFlipEnable() {
        return SharedPreferencesUtil.getInstance().getBoolean("volumeFlip", true);
    }

    /**
     * 保存全局生效的阅读字体大小
     *
     * @param fontSizePx
     */
    public void saveFontSize(int fontSizePx) {
        saveFontSize("", fontSizePx);
    }

    public void saveHiraganaFontSize(int fontSizePx) {
        saveFontSize("hiragana", fontSizePx);
    }
    /**
     * 保存书籍阅读字体大小
     *
     * @param bookId     需根据bookId对应，避免由于字体大小引起的分页不准确
     * @param fontSizePx
     * @return
     */
    public void saveFontSize(String bookId, int fontSizePx) {
        // 书籍对应
        SharedPreferencesUtil.getInstance().putInt(getFontSizeKey(bookId), fontSizePx);
    }

    public void saveReadTheme(int theme) {
        SharedPreferencesUtil.getInstance().putInt("readTheme", theme);
    }

    public int getReadTheme() {
        if (SharedPreferencesUtil.getInstance().getBoolean(Constant.ISNIGHT, false)) {
            return ThemeManager.NIGHT;
        }
        return SharedPreferencesUtil.getInstance().getInt("readTheme", 3);
    }

    public void saveThemeColor(int color) {
        SharedPreferencesUtil.getInstance().putInt("themeColor", color);
    }

    public List<BookMark> getBookMarks(String bookId) {
        return SharedPreferencesUtil.getInstance().getObject(getBookMarksKey(bookId), ArrayList.class);
    }

    public int getThemeColor() {
        return SharedPreferencesUtil.getInstance().getInt("themeColor", Color.rgb(0,0,0));
    }

    public void saveCategoryPagePosition(int position) {
        SharedPreferencesUtil.getInstance().putInt("categoryPagePosition", position);
    }

    public int getCategoryPagePosition() {
        return SharedPreferencesUtil.getInstance().getInt("categoryPagePosition", 0);
    }

    public void clearBookMarks(String bookId) {
        SharedPreferencesUtil.getInstance().remove(getBookMarksKey(bookId));
    }

    private String getBookMarksKey(String bookId) {
        return bookId + "-marks";
    }

    public boolean addBookMark(String bookId, BookMark mark) {
        List<BookMark> marks = SharedPreferencesUtil.getInstance().getObject(getBookMarksKey(bookId), ArrayList.class);
        if (marks != null && marks.size() > 0) {
            for (BookMark item : marks) {
                if (item.chapter == mark.chapter && item.page == mark.page) {
                    return false;
                }
            }
        } else {
            marks = new ArrayList<>();
        }
        marks.add(mark);
        SharedPreferencesUtil.getInstance().putObject(getBookMarksKey(bookId), marks);
        return true;
    }

}
